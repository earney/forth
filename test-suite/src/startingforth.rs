// examples from the book Starting Forth..
//

#[cfg(test)]
mod tests {
    //use super::*;
    use forth::forth_compiler::ForthCompiler;
    use forth::stack_machine::GasLimit;
    //  use forth::ForthError;

    /*
      macro_rules! clean_stack (
          {$fc: ident, $expr: expr, $result: expr} => {
              $fc.sm.st.data_stack.clear();
              $fc.execute_string($expr, GasLimit::Limited(100)).unwrap();
              assert_eq!($fc.sm.st.data_stack, $result);
          }
      );
    */
    macro_rules! output (
        {$fc: ident, $expr: expr, $result: expr} => {
            assert_eq!($fc.execute_string($expr, GasLimit::Limited(400)).unwrap(), $result);
        }
    );

    /*
    macro_rules! error (
        {$fc: ident, $expr: expr, $result: expr} => {
            $fc.sm.st.data_stack.clear();
            assert_eq!($fc.execute_string($expr, GasLimit::Limited(100)).unwrap_err(), $result);
        }
    );
    */

    macro_rules! run (
        {$fc: ident, $expr: expr} => {
            $fc.execute_string($expr, GasLimit::Limited(100)).unwrap()
        }
    );

    #[test]
    fn chapter2() {
        // page 43
        let mut fc = ForthCompiler::new();

        output!(fc, r"22 4 /MOD . .", "5 2 ");
    }

    #[test]
    fn chapter5() {
        let mut fc = ForthCompiler::new();

        // page 117
        output!(fc, r"2000 34 100 */ .", "680 ");

        // page 118
        output!(fc, "227 32 10 */ .", "726 ");

        run!(fc, ": R% 10 */  5 +  10 / ;");
        output!(fc, "227 32 R% .", "73 ");

        // page 119
        output!(fc, "171 2 3 */ .", "114 ");

        // page 122
        run!(fc, ": PI DUP * 31416 10000 */ ;");
        output!(fc, "10 PI .", "314 ");

        run!(fc, ": PI DUP * 355 113 */ ;");
        output!(fc, "10 PI .", "314 ");
    }

    #[test]
    fn chapter6() {
        let mut fc = ForthCompiler::new();

        // page 127

        run!(fc, r#": test 10 0 do cr ." Hello" loop ; "#);
        output!(
            fc,
            "test",
            "\nHello\nHello\nHello\nHello\nHello\nHello\nHello\nHello\nHello\nHello"
        );

        // page 128
        run!(fc, r#": test 3 0 do cr ." I'm going loopy! " loop ;"#);
        output!(
            fc,
            "test",
            "\nI'm going loopy! \nI'm going loopy! \nI'm going loopy! "
        );

        // page 130
        run!(fc, r#": decade 10 0 do I . loop ;"#);
        output!(fc, "decade", "0 1 2 3 4 5 6 7 8 9 ");

        run!(fc, r#": sample -243 -250 do I . loop ; "#);
        output!(fc, "sample", "-250 -249 -248 -247 -246 -245 -244 ");

        run!(fc, r#": mult CR 11 1 DO DUP I * . LOOP DROP ;"#);
        output!(fc, "7 mult", "\n7 14 21 28 35 42 49 56 63 70 ");

        // page 131

        //run!(fc, ": 2/ 1 RSHIFT ;");
        //run!(fc, ": R% 50 / 1+ 2/ ;");
        run!(fc, ": R% 10 */  5 + 10 / ;");

        run!(
            fc,
            r#"
                : compound ( amt int -- )
                    swap 4 1 do ." YEAR " I . 3 spaces
                    2dup R% + DUP ." BALANCE " . CR LOOP 2DROP ;"#
        );
        output!(
            fc,
            "1000 6 compound",
            "YEAR 1    BALANCE 1060 \nYEAR 2    BALANCE 1124 \nYEAR 3    BALANCE 1191 \n"
        );
    }
}
