use std::fs::File;
use std::io::{Error, Read, Write};

pub struct Block {
    pub data: [[u8; 64]; 16],
    number: usize,
}

impl Block {
    pub fn new(number: usize) -> Block {
        Block {
            data: [[0u8; 64]; 16],
            number,
        }
    }

    fn list(&self) {
        self.display();
    }

    // load and process the contents of a block into memory
    pub fn read(&mut self) {
        let filename = format!("blockfs/block{}.dat", self.number);

        let mut file = match File::open(filename) {
            Ok(x) => x,
            Err(x) => panic!("{}", x),
        };
        for i in 0..16 {
            let mut buf = [32u8; 64]; // 64 spaces
            let _size = file.read(&mut buf);
            self.data[i] = buf;
        }
    }

    pub fn save(&mut self, number: Option<usize>) -> Result<bool, Error> {
        self.number = match number {
            Some(x) => x,
            None => self.number,
        };
        let filename = format!("blockfs/block{}.dat", self.number);
        let mut file = match File::open(filename) {
            Ok(x) => x,
            Err(x) => panic!("{}", x),
        };
        // let s: [&[]; 16];
        for row in &self.data {
            let _size = file.write(row);
        }
        Ok(true)
    }

    pub fn wipe(&mut self) -> Result<bool, Error> {
        self.data = [[0u8; 64]; 16];
        self.save(None)
    }

    pub fn display(&self) -> String {
        let mut s = String::new();
        for i in 0..16 {
            s += format!("{}: {:?}\n", i, self.data[i]).as_str();
        }
        s
    }
}
