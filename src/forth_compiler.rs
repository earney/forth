use super::error::ForthError;
use super::stack_machine::GasLimit;
use super::stack_machine::Opcode;
use super::stack_machine::StackMachine;
use std::collections::HashMap;
use std::convert::TryFrom;
use std::convert::TryInto;

pub use super::stack_machine::HandleTrap;
pub use super::stack_machine::StackMachineError;
pub use super::stack_machine::TrapHandled;
pub use super::stack_machine::TrapHandler;

type StackType = i16;
/// This Enum lists the token types that are used by the Forth interpreter
#[derive(Debug)]
pub enum Token {
    Number(StackType),
    Command(String),
    Constant(String),
    Comment(String),
    Colon(String),
    CHAR(char),
    Text(String),
    SDoubleTick(String),
    FormattedNumericOutput(String),
    Create(String),
    SemiColon,
    End,
    Period,
    Error(String),
}

pub struct ForthCompiler {
    // This is the Stack Machine processor that runs the compiled Forth instructions
    pub sm: StackMachine,
    // These are the words that we know how to work with regardless, things like DROP, *, etc
    pub intrinsic_words: HashMap<&'static str, Opcode>,
    // This is where we remember where we put compiled words in the *memory* of the StackMachine
    // We run the interactive opcodes after these compiled words, and then erase the memory after
    // the compiled words again for the next batch of interactive opcodes.
    pub word_addresses: HashMap<String, usize>,
    // This is the location in memory that points to the location after the last compiled opcode
    // So its an ideal place to run interactive compiled opcodes
    last_function: usize,
}

impl Default for ForthCompiler {
    fn default() -> Self {
        Self::new()
    }
}

impl ForthCompiler {
    pub fn new() -> ForthCompiler {
        ForthCompiler {
            sm: StackMachine::new(),
            intrinsic_words: HashMap::from([
                (".", Opcode::PRINT),
                ("EMIT", Opcode::EMIT),
                ("CR", Opcode::CR),
                ("?", Opcode::QuestionMark),
                ("SPACE", Opcode::SPACE),
                ("SPACES", Opcode::SPACES),
                (".R", Opcode::RightAlign),
                ("KEY", Opcode::KEY),
                //variable
                //  ("COUNT", Opcode::COUNT),
                //  ("TYPE", Opcode::TYPE),
                // stack ops
                ("DROP", Opcode::DROP),
                ("SWAP", Opcode::SWAP),
                ("OVER", Opcode::OVER),
                ("DUP", Opcode::DUP),
                ("2DROP", Opcode::TWODROP),
                ("2DUP", Opcode::TWODUP),
                ("2OVER", Opcode::TWOOVER),
                ("2SWAP", Opcode::TWOSWAP),
                ("2ROT", Opcode::TWOROT),
                ("DEPTH", Opcode::DEPTH),
                ("?DUP", Opcode::QuestionDup),
                ("ROT", Opcode::ROT),
                // return stack ops
                (">R", Opcode::ToR),
                ("R>", Opcode::RFrom),
                ("R@", Opcode::RFetch),
                // math ops
                ("NEGATE", Opcode::NEGATE),
                ("+", Opcode::ADD),
                ("-", Opcode::SUB),
                ("*", Opcode::MUL),
                ("/", Opcode::DIV),
                ("MOD", Opcode::MOD),
                ("*/MOD", Opcode::MultDivMOD),
                ("/MOD", Opcode::DivMOD),
                ("*/", Opcode::STARSLASH),
                (">", Opcode::GT),
                ("<", Opcode::LT),
                ("=", Opcode::EQ),
                ("NOT", Opcode::NOT),
                ("AND", Opcode::AND),
                ("OR", Opcode::OR),
                ("XOR", Opcode::XOR),
                ("0<", Opcode::ZeroLess),
                ("0=", Opcode::ZeroEqual),
                ("0>", Opcode::ZeroGreater),
                ("1+", Opcode::OnePlus),
                ("1-", Opcode::OneMinus),
                ("2+", Opcode::TwoPlus),
                ("2-", Opcode::TwoMinus),
                ("LSHIFT", Opcode::LShift),
                ("RSHIFT", Opcode::RShift),
                ("TRAP", Opcode::TRAP),
                ("SETSEED", Opcode::SETSEED),
                ("RAND", Opcode::RAND),
                //variable ops
                ("!", Opcode::BANG),
                ("@", Opcode::AT),
                (",", Opcode::Comma),
                ("CELLS", Opcode::CELLS),
                ("CHARS", Opcode::CHARS),
                ("CHAR+", Opcode::CHARPLUS),
                ("CMOVE", Opcode::CMOVE),
                ("ALLOT", Opcode::ALLOCATE),
                // memory opcodes.
                ("C@", Opcode::CAt),
                ("C!", Opcode::CBang),
            ]),
            word_addresses: HashMap::new(),
            last_function: 0,
        }
    }
}

/// This Enum determines whether the Forth interpreter is in Interpreting mode or Compiling mode
#[derive(Debug, PartialEq)]
enum Mode {
    Interpreting,
    Compiling(String),
}

// This struct tracks information for Forth IF statements
#[derive(Debug)]
struct DeferredIfStatement {
    if_location: usize,
    else_location: Option<usize>,
}

impl DeferredIfStatement {
    pub fn new(if_location: usize) -> DeferredIfStatement {
        DeferredIfStatement {
            if_location,
            else_location: None,
        }
    }
}

struct DeferredDoStatement {
    do_location: usize,
    incr_location: usize,
    comparison_location: usize,
}

impl DeferredDoStatement {
    pub fn new(
        do_location: usize,
        incr_location: usize,
        comparison_location: usize,
    ) -> DeferredDoStatement {
        DeferredDoStatement {
            do_location,
            incr_location,
            comparison_location,
        }
    }
}

struct DeferredWhileStatement {
    begin_location: StackType,
    condition_location: StackType,
}

impl DeferredWhileStatement {
    pub fn new(begin_location: StackType, condition_location: StackType) -> DeferredWhileStatement {
        DeferredWhileStatement {
            begin_location,
            condition_location,
        }
    }
}

impl ForthCompiler {
    // Take a string containing Forth words and turn it into a list of Forth tokens
    fn tokenize_string(&self, s: &str) -> Result<Vec<Token>, ForthError> {
        let mut tv = Vec::new();

        //  remove comments ie ( \ this is a comment )
        let mut st = Vec::<String>::new();
        for line in s.split('\n') {
            st.push(line.split("\\ ").take(1).next().unwrap_or("").to_string());
        }

        let st = st.join(" ");

        let s = st
            .split_whitespace()
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .collect::<Vec<_>>();

        let mut string_iter = s.iter();

        //  println!("{:?}", s);
        loop {
            match string_iter.next() {
                // If no more text in the string, then return what we have tokenized
                None => return Ok(tv),
                // If we have some text to process, then process it
                Some(string_token) => {
                    // Try to convert it to a number
                    match string_token.parse::<StackType>() {
                        // We found a number, then return it as a number token
                        Ok(n) => tv.push(Token::Number(n)),
                        // Wasn't a number, treat it as a *word*
                        Err(_) => match string_token.to_uppercase().as_str() {
                            // If its a colon, create a colon token
                            ":" => match &string_iter.next() {
                                // If we found a token, then we need to grab the next bit of text so we know what Forth word is being compiled
                                Some(next_token) => tv.push(Token::Colon(next_token.to_string())),
                                // There has to be something after the colon, so this is an error since we didn't find anything
                                None => {
                                    return Err(ForthError::InvalidSyntax(String::from(
                                        "No token after :, but one is needed to compile",
                                    )))
                                }
                            },
                            "CHAR" => match &string_iter.next() {
                                Some(c) => match c.len() {
                                    1 => match c.chars().next() {
                                        Some(x) => tv.push(Token::CHAR(x)),
                                        None => todo!(),
                                    },
                                    _ => {
                                        return Err(ForthError::InvalidSyntax(String::from(
                                            "No single character after CHAR keyword",
                                        )))
                                    }
                                },
                                None => {
                                    return Err(ForthError::InvalidSyntax(String::from(
                                        "No single character after CHAR keyword",
                                    )))
                                }
                            },
                            "CONSTANT" => match &string_iter.next() {
                                Some(con) => tv.push(Token::Constant(con.to_string())),
                                None => {
                                    return Err(ForthError::InvalidSyntax(String::from(
                                        "No constant name after CONSTANT keyword",
                                    )));
                                }
                            },
                            "VARIABLE" | "CREATE" => match &string_iter.next() {
                                Some(var) => {
                                    //allocate.clear();
                                    tv.push(Token::Create(var.to_string()))
                                }
                                None => {
                                    return Err(ForthError::InvalidSyntax(format!(
                                        "No name after {} keyword",
                                        string_token.to_uppercase()
                                    )));
                                }
                            },
                            // Create a semicolon token
                            ";" => tv.push(Token::SemiColon),
                            "S\"" => {
                                // beginning of array of chars stored in memory.
                                let mut text: Vec<String> = Vec::new();
                                loop {
                                    let mut mystr: String = match &string_iter.next() {
                                        Some(x) => x.to_string(),
                                        _ => todo!(),
                                    };
                                    if !mystr.ends_with('\"') {
                                        text.push(mystr.clone());
                                    } else {
                                        mystr.pop(); // remove " from the end of the string.
                                        text.push(mystr.clone());
                                        break;
                                    }
                                }
                                tv.push(Token::SDoubleTick(text.join(" ")))
                            }
                            ".\"" => {
                                // this is text.
                                // text
                                let mut text: Vec<String> = Vec::new();
                                loop {
                                    let mut mystr: String = match &string_iter.next() {
                                        Some(x) => x.to_string(),
                                        _ => todo!(),
                                    };
                                    if !mystr.ends_with('\"') {
                                        text.push(mystr.clone());
                                    } else {
                                        mystr.pop(); // remove " from the end of the string.
                                        text.push(mystr.clone());
                                        break;
                                    }
                                }
                                tv.push(Token::Text(text.join(" ")))
                            }
                            ".(" => {
                                // this is text.
                                // text
                                let mut text: Vec<String> = Vec::new();
                                loop {
                                    let mut mystr: String = match &string_iter.next() {
                                        Some(x) => x.to_string(),
                                        _ => todo!(),
                                    };
                                    if !mystr.ends_with(')') {
                                        text.push(mystr.clone());
                                    } else {
                                        mystr.pop(); // remove " from the end of the string.
                                        text.push(mystr.clone());
                                        break;
                                    }
                                }
                                tv.push(Token::Text(text.join(" ")))
                            }
                            "<#" => {
                                let mut text: Vec<String> = Vec::new();
                                loop {
                                    let mystr: String = match &string_iter.next() {
                                        Some(x) => x.to_string(),
                                        _ => todo!(),
                                    };
                                    if !mystr.ends_with("#>") {
                                        text.push(mystr.clone());
                                    } else {
                                        //mystr.pop(); // remove " from the end of the string.
                                        //text.push(mystr.clone());
                                        break;
                                    }
                                }
                                tv.push(Token::FormattedNumericOutput(text.join(" ")))
                            }
                            "(" => {
                                // stuff between ( and ) are comments
                                let mut text: Vec<String> = Vec::new();
                                loop {
                                    let mut mystr: String = match &string_iter.next() {
                                        Some(x) => x.to_string(),
                                        _ => todo!(),
                                    };
                                    if !mystr.ends_with(')') {
                                        text.push(mystr.clone());
                                    } else {
                                        mystr.pop(); // remove " from the end of the string.
                                        text.push(mystr.clone());
                                        break;
                                    }
                                }
                                tv.push(Token::Comment(text.join(" ")))
                            }
                            "\\" => {
                                // rest of line comment      ie  \ this is a comment
                                let mut text: Vec<String> = Vec::new();
                                while let Some(x) = &string_iter.next() {
                                    text.push(x.to_string());
                                }
                                tv.push(Token::Comment(text.join(" ")))
                            }
                            // Whatever else, assume its a Forth word
                            _ => {
                                // if is_variable {
                                //     allocate.push(string_token.to_string());
                                // } else {
                                tv.push(Token::Command(string_token.to_string()))
                                // }
                            }
                        },
                    };
                }
            }
        }
    }

    fn compile_token_vector_compile_and_remove_word_definitions(
        &mut self,
        token_vector: &[Token],
    ) -> Result<Vec<Opcode>, ForthError> {
        // This is the interactive compiled token list
        let mut tvi = Vec::new();
        // This tracks whethere we are interpreting or compiling right now
        let mut mode = Mode::Interpreting;
        // This is where we start compiling the latest segment of word/interactive tokens
        let mut starting_position = 0;

        //println!(
        //    "compile_token_vector_compile_and_remove_word_definitions Compiling Forth tokens {:?}",
        //    token_vector
        //);
        // So, for every token we have been passed, check what it is...
        for i in 0..token_vector.len() {
            // println!("{:?}", token_vector[i]);
            match &token_vector[i] {
                // :
                Token::Colon(s) => {
                    // Found Colon, so the user wants to compile a word presumably
                    match mode {
                        // If we are currently interpreting, then we can safely switch to compiling
                        Mode::Interpreting => {
                            // Make sure there is something to compile...
                            if i > starting_position {
                                // We end before the current token
                                // Compile whatever appeared before this compile statement
                                tvi.append(
                                    &mut self.compile_token_vector(
                                        &token_vector[starting_position..i],
                                    )?,
                                );
                            }
                            // Start compiling again after this token
                            starting_position = i + 1;
                            // Switch to compiling mode, remmeber the word we are trying to compile
                            mode = Mode::Compiling(String::from(s));
                        }
                        // We are already in compiling mode, so getting a colon is a syntax error
                        Mode::Compiling(_) => {
                            return Err(ForthError::InvalidSyntax(
                                "Second colon before semicolon".to_string(),
                            ));
                        }
                    }
                }
                // ;
                Token::SemiColon => {
                    match mode {
                        // We are in interpreting mode, this is a syntax error
                        Mode::Interpreting => {
                            return Err(ForthError::InvalidSyntax(
                                "Semicolon before colon".to_string(),
                            ));
                        }
                        // We have found the end of the word definition, so compile to opcodes and put into memory...
                        Mode::Compiling(s) => {
                            // Remove anything extraneous from the end of the opcode array (*processor memory*),
                            // typically previous immediate mode tokens
                            self.sm.st.opcodes.resize(self.last_function, Opcode::NOP);

                            // Get the compiled assembler from the token vector
                            // stop compiling before the ending token
                            let mut compiled =
                                self.compile_token_vector(&token_vector[starting_position..i])?;
                            // Put the return OpCode onto the end
                            compiled.push(Opcode::RET);
                            // The current function start is the end of the last function
                            let function_start = self.last_function;
                            // Move last function pointer
                            self.last_function += compiled.len();
                            // + the function to the opcode memory
                            self.sm.st.opcodes.append(&mut compiled);
                            // Remember where to find it...
                            self.word_addresses.insert(s.to_uppercase(), function_start);
                            // start compiling again after this token
                            starting_position = i + 1;
                            // Switch back to interpreting mode
                            mode = Mode::Interpreting;
                            //println!("Token Memory {:?}", self.sm.st.opcodes);
                            //println!("Word +resses {:?}", self.word_addresses);
                            //println!("Last function {}", self.last_function);
                        }
                    }
                }
                _ => (),
            }
        }

        // Check for an error condition and report it
        // If we are not in interpreting mode when we have processed all the Forth tokens, then that's an error
        if mode != Mode::Interpreting {
            return Err(ForthError::MissingSemicolonAfterColon);
        }

        // Compile any tokens that remain after processing
        let mut compiled = self.compile_token_vector(&token_vector[starting_position..])?;
        tvi.append(&mut compiled);
        // We need to return after running the interactive opcodes, so put the return in now
        tvi.push(Opcode::RET);

        // Return the interactive tokens, the compiled ones are already in memory
        Ok(tvi)
    }

    fn compile_token_vector(&mut self, token_vector: &[Token]) -> Result<Vec<Opcode>, ForthError> {
        // Stack of if statements, they are deferred until the THEN Forth word
        let mut deferred_if_statements = Vec::new();
        // used for do statements.
        let mut deferred_do_statements = Vec::new();
        // used for while statements
        let mut deferred_while_statements = Vec::new();
        // List of compiled processor opcodes that we are building up
        let mut tv: Vec<Opcode> = Vec::new();

        // Go through all the Forth tokens and turn them into processor Opcodes (for our StackMachine emulated processor)
        for t in token_vector.iter() {
            println!("{:?}", t);
            match t {
                Token::Number(n) => {
                    // Numbers get pushed as a LDI opcode
                    tv.push(Opcode::LDI(*n));
                }
                Token::CHAR(c) => {
                    tv.push(Opcode::CHAR(*c));
                }
                Token::Constant(c) => {
                    match self.sm.st.memory.get_word_addr(c.to_uppercase()) {
                        Some(_) => return Err(ForthError::ConstantAlreadyExists(c.to_string())),
                        None => {
                            self.sm.st.memory.add_variable(c.to_uppercase(), true);
                        }
                    }
                    tv.push(Opcode::SETCONSTANT(c.to_uppercase()));
                }
                Token::Text(t) => {
                    tv.push(Opcode::PRINTTEXT(t.to_string()));
                }
                Token::FormattedNumericOutput(t) => {
                    tv.push(Opcode::FormattedNumericOutput(t.to_string()));
                }
                Token::Create(variable) => {
                    match self.sm.st.memory.get_word_addr(variable.to_uppercase()) {
                        Some(_) => {
                            return Err(ForthError::VariableAlreadyExists(variable.to_string()))
                        }
                        None => {
                            self.sm
                                .st
                                .memory
                                .add_variable(variable.to_uppercase(), false);
                        }
                    }
                    tv.push(Opcode::CREATE(variable.to_uppercase()));
                }
                Token::Command(s) => {
                    // Remember where we are in the list of opcodes in case we hit a IF statement, LOOP etc...
                    let current_instruction = tv.len();

                    //println!("{:?}", tv);
                    match s.to_uppercase().as_ref() {
                        "BEGIN" => {
                            let begin_location = current_instruction as StackType;
                            deferred_while_statements
                                .push(DeferredWhileStatement::new(begin_location, 0));
                        }
                        "UNTIL" => {
                            if let Some(x) = deferred_while_statements.pop() {
                                let pos = tv.len() as StackType;
                                tv.push(Opcode::LDI(x.begin_location - pos - 1));
                                tv.push(Opcode::JRZ);
                            }
                        }
                        "AGAIN" => {
                            if let Some(x) = deferred_while_statements.pop() {
                                let pos = tv.len() as StackType;
                                tv.push(Opcode::LDI(x.begin_location - pos - 1));
                                tv.push(Opcode::JMP);
                            }
                        }
                        "WHILE" => {
                            let condition_location = current_instruction as StackType;
                            tv.push(Opcode::LDI(0));
                            tv.push(Opcode::JRZ);
                            let len = deferred_while_statements.len();
                            deferred_while_statements[len - 1].condition_location =
                                condition_location;
                        }
                        "REPEAT" => {
                            if let Some(x) = deferred_while_statements.pop() {
                                // jump back up to the begin statements so we can check the condtion again...
                                let pos = tv.len() as StackType;
                                tv.push(Opcode::LDI(x.begin_location - pos - 1));
                                tv.push(Opcode::JMP);

                                let repeat_location: StackType = tv.len() as StackType - 1;
                                // update while jump location sinc we now know where to jump to if condition is false;
                                tv[x.condition_location as usize] = Opcode::LDI(
                                    (repeat_location - x.condition_location) as StackType,
                                );
                            }
                        }
                        "I" => {
                            if !deferred_do_statements.is_empty() {
                                tv.extend_from_slice(&[
                                    Opcode::RFrom,
                                    Opcode::RFrom,
                                    Opcode::TWODUP,
                                    Opcode::ToR,
                                    Opcode::ToR,
                                    Opcode::SWAP,
                                    Opcode::DROP,
                                    Opcode::DEBUG("I".to_string()),
                                ])
                            } else {
                                todo!();
                            }
                        }
                        "?DO" | "DO" => {
                            // while i < end  (true (1)

                            let do_location = tv.len();
                            tv.push(Opcode::DEBUG(" in DO".to_string()));
                            tv.push(Opcode::TWODUP); // copy the end and start values
                            tv.push(Opcode::ToR); // put the end on the return stack
                            tv.push(Opcode::ToR); // put the start on the return stack

                            //  let start_location = tv.len();
                            let comparison_location = tv.len();
                            tv.push(Opcode::GT); // do the comparison.  will return 0 when false
                            let incr_location = tv.len();
                            tv.push(Opcode::LDI(0)); // jump to end of do loop (placeholder)
                                                     // tv.push(Opcode::SWAP);
                            tv.push(Opcode::JRZ); // will jump to instruction after loop if comparison is 0.
                            deferred_do_statements.push(DeferredDoStatement::new(
                                do_location,
                                incr_location,
                                comparison_location,
                            ));
                        }
                        "LOOP" => {
                            if let Some(x) = deferred_do_statements.pop() {
                                //update the instruction so it knows where to jump to when the loop is done.
                                //tv[x.do_location]=Opcode::LDI(current_instruction as StackType);

                                //get the index from the return stack
                                tv.push(Opcode::DEBUG("IN loop".to_string()));
                                tv.push(Opcode::RFrom);
                                tv.push(Opcode::RFrom);
                                //add one to it
                                tv.push(Opcode::LDI(1));
                                tv.push(Opcode::ADD);

                                let pos: StackType = tv.len() as StackType;
                                tv.push(Opcode::LDI(x.do_location as StackType - pos - 1));
                                tv.push(Opcode::JR);
                                let pos: StackType = tv.len() as StackType;
                                tv[x.incr_location] =
                                    Opcode::LDI(pos - 1 - x.incr_location as StackType);
                                tv.push(Opcode::RFrom); // remove end value
                                tv.push(Opcode::RFrom); // rmeove end value
                                tv.push(Opcode::RET);

                                // if all goes well, we are done with updating the loop.
                            } else {
                                return Err(ForthError::InvalidSyntax(
                                    "LOOP without DO".to_owned(),
                                ));
                            }
                        }
                        "+LOOP" => {
                            if let Some(x) = deferred_do_statements.pop() {
                                //update the instruction so it knows where to jump to when the loop is done.
                                //tv[x.do_location]=Opcode::LDI(current_instruction as StackType);

                                //assume last element put on the stack is the increment
                                let code = tv[tv.len() - 1].clone();
                                let index: Vec<Opcode> = match code {
                                    Opcode::LDI(y) => {
                                        if y < 0 {
                                            // when we have a negative increment
                                            tv[x.comparison_location] = Opcode::LT;
                                        }
                                        vec![Opcode::LDI(y)]
                                    }
                                    Opcode::DEBUG(s) => match s.as_str() {
                                        "I" => {
                                            vec![Opcode::DUP, Opcode::DEBUG("I".to_string())]
                                        }
                                        _ => todo!(),
                                    },
                                    _ => todo!(), // shouldn't ever get here.
                                };
                                tv.push(Opcode::RFrom);
                                tv.push(Opcode::RFrom);
                                //add one to it

                                tv.extend_from_slice(&index);
                                tv.push(Opcode::ADD);
                                //tv.push(Opcode::RFrom);
                                // put it back on the return stack for the next type through the loop (ie comparison, etc)

                                let pos: StackType = tv.len() as StackType;
                                tv.push(Opcode::LDI(x.do_location as StackType - pos - 1));
                                tv.push(Opcode::JR);
                                let pos: StackType = tv.len() as StackType;
                                tv[x.incr_location] =
                                    Opcode::LDI(pos - 1 - x.incr_location as StackType);
                                tv.push(Opcode::RFrom); // remove end value
                                tv.push(Opcode::RFrom); // rmeove end value
                                tv.push(Opcode::RET);

                                // if all goes well, we are done with updating the loop.
                            } else {
                                return Err(ForthError::InvalidSyntax(
                                    "LOOP without DO".to_owned(),
                                ));
                            }
                        }
                        "IF" => {
                            deferred_if_statements
                                .push(DeferredIfStatement::new(current_instruction));
                            //println!("(IF)Deferred If Stack {:?}", deferred_if_statements);
                            // put in placeholders just in case we need to jump..
                            tv.push(Opcode::LDI(0));
                            tv.push(Opcode::JRZ);
                        }
                        "ELSE" => {
                            if let Some(x) = deferred_if_statements.last_mut() {
                                x.else_location = Some(current_instruction);
                                //println!("(ELSE) Deferred If Stack {:?}", deferred_if_statements);
                                // put in placeholder
                                tv.push(Opcode::LDI(0));
                                tv.push(Opcode::JR);
                            } else {
                                return Err(ForthError::InvalidSyntax(
                                    "ELSE without IF".to_owned(),
                                ));
                            }
                        }
                        "THEN" => {
                            // This only works if there isn't an ELSE statement, it needs to jump differently if there is an ELSE statement

                            //println!("(THEN) Deferred If Stack {:?}", deferred_if_statements);
                            if let Some(x) = deferred_if_statements.pop() {
                                //println!("(if let Some(x)) Deferred If Stack {:?}", x);
                                let if_jump_location = x.if_location;
                                let if_jump_offset = match x.else_location {
                                    None => (current_instruction as usize
                                        - (x.if_location + 1) as usize)
                                        .try_into()
                                        .unwrap(),
                                    Some(el) => (current_instruction as usize - el as usize + 1)
                                        .try_into()
                                        .unwrap(),
                                };
                                let (else_jump_location, else_jump_offset): (
                                    Option<usize>,
                                    Option<StackType>,
                                ) = match x.else_location {
                                    Some(x) => (
                                        Some(x),
                                        Some(
                                            StackType::try_from(
                                                current_instruction as usize - (x + 1) as usize,
                                            )
                                            .unwrap(),
                                        ),
                                    ),
                                    None => (Some(tv.len()), None),
                                };
                                tv[if_jump_location] = Opcode::LDI(if_jump_offset);

                                if let (Some(location), Some(offset)) =
                                    (else_jump_location, else_jump_offset)
                                {
                                    tv[location] = Opcode::LDI(offset);
                                }
                            } else {
                                return Err(ForthError::InvalidSyntax(
                                    "THEN without IF".to_owned(),
                                ));
                            }
                        }
                        _ => {
                            if let Some(offset) = self.word_addresses.get(&s.to_uppercase()) {
                                tv.push(Opcode::LDI(*offset as StackType));
                                tv.push(Opcode::CALL);
                            } else if let Some(ol) =
                                self.intrinsic_words.get::<str>(&s.to_uppercase())
                            {
                                tv.push(ol.clone());
                            } else if self.sm.st.memory.contains_word(s.to_string()) {
                                // see if this is a variable reference..
                                let word_addr: usize =
                                    match self.sm.st.memory.get_word_addr(s.to_string()) {
                                        Some(x) => x,
                                        _ => todo!(),
                                    };

                                let memory_address = self.sm.st.memory.cell_type_array[word_addr]
                                    .memory_address
                                    .unwrap();
                                println!(
                                    "word:{} word_addr:{}, memory_address:{}",
                                    s, word_addr, memory_address
                                );
                                if self.sm.st.memory.cell_type_array[word_addr].isconstant {
                                    //let addr =
                                    //    self.sm.st.memory.cell_type_array[word_addr].memory_address;
                                    let value =
                                        self.sm.st.memory.get_memory_address(memory_address);
                                    tv.push(Opcode::LDI(value[0]));
                                } else {
                                    tv.push(Opcode::LDI(memory_address as StackType));
                                }
                            } else {
                                return Err(ForthError::UnknownToken(s.to_string()));
                            }
                        }
                    }
                }
                Token::SDoubleTick(s) => {
                    tv.push(Opcode::SDoubleTick(s.to_string()));
                }
                Token::Period => {
                    tv.push(Opcode::PRINT);
                    //print out TOS and remove element from stack;
                }
                Token::Comment(_s) => {} //skip for now. can ignore comments.
                Token::Colon(_) => {
                    panic!("Colon should never reach this function");
                }
                Token::SemiColon => {
                    panic!("SemiColon should never reach this function");
                }
                Token::End => {
                    panic!("Token::End not coded yet");
                }
                Token::Error(_) => {
                    panic!("Token::Error not coded yet");
                }
            }
        }

        //println!("Compiled Codes {:?}", tv);
        //println!("Total size of Codes {:?}", tv.len());
        if !deferred_if_statements.is_empty() {
            Err(ForthError::InvalidSyntax("If without then".to_string()))
        } else {
            Ok(tv)
        }
    }

    fn execute_token_vector(
        &mut self,
        token_vector: &[Token],
        gas_limit: GasLimit,
    ) -> Result<String, ForthError> {
        let mut ol = self.compile_token_vector_compile_and_remove_word_definitions(token_vector)?;
        //println!("Compiled Opcodes: {:?}", ol);
        self.sm.st.opcodes.resize(self.last_function, Opcode::NOP);
        self.sm.st.opcodes.append(&mut ol);
        match self.sm.execute(self.last_function, gas_limit) {
            Ok(x) => Ok(x),
            Err(x) => Err(x.into()),
        }
        //println!("Total opcodes defined: {}", self.sm.st.opcodes.len());
        //println!("Total opcodes executed: {}", self.sm.st.gas_used());
    }

    pub fn execute_string(&mut self, s: &str, gas_limit: GasLimit) -> Result<String, ForthError> {
        let tv = self.tokenize_string(s)?;
        self.execute_token_vector(&tv, gas_limit)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_allocate() {
        // let mut fc = ForthCompiler::new();
    }

    #[test]
    fn test_emit() {
        let mut fc = ForthCompiler::new();

        // push 4 to return stack and then back to the parameter stack.
        assert_eq!(
            fc.execute_string(
                "CHAR F EMIT CHAR O EMIT CHAR R EMIT CHAR T EMIT CHAR H EMIT",
                GasLimit::Limited(100)
            )
            .unwrap(),
            "FORTH".to_string()
        );

        match fc.execute_string("CHAR EMIT", GasLimit::Limited(100)) {
            Err(ForthError::InvalidSyntax(_)) => (),
            r => panic!("Incorrect error type returned {:?}", r),
        }
    }

    #[test]
    fn test_syntax_errors() {
        let mut fc = ForthCompiler::new();

        // push 4 to return stack and then back to the parameter stack.
        match fc.execute_string("2 Loop", GasLimit::Limited(100)) {
            Err(ForthError::InvalidSyntax(_)) => (),
            r => panic!("Incorrect error type returned {:?}", r),
        }

        match fc.execute_string("2 +Loop", GasLimit::Limited(100)) {
            Err(ForthError::InvalidSyntax(_)) => (),
            r => panic!("Incorrect error type returned {:?}", r),
        }
    }

    #[test]
    fn test_plus_loop() {
        let mut fc = ForthCompiler::new();

        // push 4 to return stack and then back to the parameter stack.
        fc.execute_string(": PJ  50 0 do i . 5 +loop ;", GasLimit::Limited(100))
            .unwrap();
        assert_eq!(
            fc.execute_string("PJ", GasLimit::Limited(500)).unwrap(),
            "0 5 10 15 20 25 30 35 40 45 ".to_string()
        );

        fc.execute_string(": PJ  100 0 do i . 10 +loop ;", GasLimit::Limited(100))
            .unwrap();
        assert_eq!(
            fc.execute_string("PJ", GasLimit::Limited(500)).unwrap(),
            "0 10 20 30 40 50 60 70 80 90 ".to_string()
        );
    }

    #[test]
    fn test_r_functions() {
        let mut fc = ForthCompiler::new();

        // push 4 to return stack and then back to the parameter stack.
        fc.execute_string("2 4 >R R>", GasLimit::Limited(100))
            .unwrap();
        assert_eq!(fc.sm.st.data_stack, vec![2, 4]);
    }

    #[test]
    fn random() {
        let mut fc = ForthCompiler::new();

        fc.execute_string("rand rand", GasLimit::Limited(100))
            .unwrap();
        assert_eq!(fc.sm.st.data_stack, vec![-7961, 4615]);

        fc.execute_string("100 setseed", GasLimit::Limited(100))
            .unwrap();
        assert_eq!(fc.sm.st.data_stack, vec![-7961, 4615]);

        fc.execute_string("rand rand", GasLimit::Limited(100))
            .unwrap();
        assert_eq!(fc.sm.st.data_stack, vec![-7961, 4615, -29662, -32274]);
    }

    #[test]
    fn test_zeroless() {
        let mut fc = ForthCompiler::new();

        fc.execute_string("1 0<", GasLimit::Limited(100)).unwrap();
        assert_eq!(fc.sm.st.data_stack, vec![0]);

        fc.execute_string("-1 0<", GasLimit::Limited(100)).unwrap();
        assert_eq!(fc.sm.st.data_stack, vec![0, 1]);
    }

    #[test]
    fn test_zeroequal() {
        let mut fc = ForthCompiler::new();

        fc.execute_string("0 0=", GasLimit::Limited(100)).unwrap();
        assert_eq!(fc.sm.st.data_stack, vec![1]);

        fc.execute_string("0=", GasLimit::Limited(100)).unwrap();
        assert_eq!(fc.sm.st.data_stack, vec![0]);
    }

    #[test]
    fn test_zerogreater() {
        let mut fc = ForthCompiler::new();

        fc.execute_string("1 0>", GasLimit::Limited(100)).unwrap();
        assert_eq!(fc.sm.st.data_stack, vec![1]);

        fc.execute_string("0 0>", GasLimit::Limited(100)).unwrap();
        assert_eq!(fc.sm.st.data_stack, vec![1, 0]);
    }

    #[test]
    fn test_comments() {
        let mut fc = ForthCompiler::new();

        fc.execute_string("( this is a comment)", GasLimit::Limited(100))
            .unwrap();
        assert_eq!(fc.sm.st.data_stack.len(), 0);

        fc.execute_string(r#"2 2 + \ comment, comment"#, GasLimit::Limited(100))
            .unwrap();
        assert_eq!(fc.sm.st.data_stack, vec![4]);
    }

    #[test]
    fn test_twoswap() {
        let mut fc = ForthCompiler::new();
        fc.execute_string("3 2 1 0 2swap", GasLimit::Limited(100))
            .unwrap();
        assert_eq!(fc.sm.st.data_stack, vec![0, 1, 2, 3]);
    }

    #[test]
    fn test_tworot() {
        let mut fc = ForthCompiler::new();
        fc.execute_string("1 2 3 4 5 6 2rot", GasLimit::Limited(100))
            .unwrap();
        assert_eq!(fc.sm.st.data_stack, vec![3, 4, 5, 6, 1, 2]);
        fc.execute_string("2rot", GasLimit::Limited(100)).unwrap();
        assert_eq!(fc.sm.st.data_stack, vec![5, 6, 1, 2, 3, 4]);
    }

    #[test]
    fn test_twoover() {
        let mut fc = ForthCompiler::new();
        fc.execute_string("1 2 3 4 5 6 2over", GasLimit::Limited(100))
            .unwrap();
        assert_eq!(fc.sm.st.data_stack, vec![1, 2, 3, 4, 5, 6, 3, 4]);
        fc.execute_string("2over", GasLimit::Limited(100)).unwrap();
        assert_eq!(fc.sm.st.data_stack, vec![1, 2, 3, 4, 5, 6, 3, 4, 5, 6]);
    }

    #[test]
    fn test_questiondup() {
        let mut fc = ForthCompiler::new();
        fc.execute_string("1 0 ?DUP", GasLimit::Limited(100))
            .unwrap();
        assert_eq!(fc.sm.st.data_stack, vec![1, 0]);

        fc.sm.st.data_stack.clear();
        fc.execute_string("1 1 ?DUP", GasLimit::Limited(100))
            .unwrap();
        assert_eq!(fc.sm.st.data_stack, vec![1, 1, 1]);
    }

    #[test]
    fn test_depth() {
        let mut fc = ForthCompiler::new();
        fc.execute_string("Depth", GasLimit::Limited(100)).unwrap();
        assert_eq!(fc.sm.st.data_stack, vec![0]);

        fc.execute_string("Depth", GasLimit::Limited(100)).unwrap();
        assert_eq!(fc.sm.st.data_stack, vec![0, 1]);
    }

    #[test]
    fn test_2drop() {
        let mut fc = ForthCompiler::new();

        fc.execute_string("2 3", GasLimit::Limited(100)).unwrap();
        assert_eq!(fc.sm.st.data_stack, vec![2, 3]);

        fc.execute_string("2drop", GasLimit::Limited(100)).unwrap();
        assert_eq!(fc.sm.st.data_stack.len(), 0);
    }

    #[test]
    fn test_1plus() {
        let mut fc = ForthCompiler::new();

        fc.execute_string("2 1+", GasLimit::Limited(100)).unwrap();
        assert_eq!(fc.sm.st.data_stack, vec![3]);
    }

    #[test]
    fn test_1minus() {
        let mut fc = ForthCompiler::new();

        fc.execute_string("2 1-", GasLimit::Limited(100)).unwrap();
        assert_eq!(fc.sm.st.data_stack, vec![1]);
    }

    #[test]
    fn test_2plus() {
        let mut fc = ForthCompiler::new();

        fc.execute_string("2 2+", GasLimit::Limited(100)).unwrap();
        assert_eq!(fc.sm.st.data_stack, vec![4]);
    }

    #[test]
    fn test_2minus() {
        let mut fc = ForthCompiler::new();

        fc.execute_string("2 2-", GasLimit::Limited(100)).unwrap();
        assert_eq!(fc.sm.st.data_stack, vec![0]);
    }

    #[test]
    fn test_spaces() {
        let mut fc = ForthCompiler::new();

        assert_eq!(
            fc.execute_string("SPACE", GasLimit::Limited(100)).unwrap(),
            " "
        );
        assert_eq!(fc.sm.st.data_stack.len(), 0);

        assert_eq!(
            fc.execute_string("3 SPACES", GasLimit::Limited(100))
                .unwrap(),
            "   "
        );
        assert_eq!(fc.sm.st.data_stack.len(), 0);
    }

    #[test]
    fn test_text() {
        let mut fc = ForthCompiler::new();

        fc.execute_string(r#" ." this is a comment""#, GasLimit::Limited(100))
            .unwrap();
        assert_eq!(fc.sm.st.data_stack.len(), 0);
    }

    #[test]
    fn test_constants() {
        let mut fc = ForthCompiler::new();

        fc.execute_string("314 constant PI", GasLimit::Limited(100))
            .unwrap();
        assert_eq!(fc.sm.st.data_stack.len(), 0);

        fc.execute_string("PI", GasLimit::Limited(100)).unwrap();
        assert_eq!(fc.sm.st.data_stack, vec![314]);
    }

    #[test]
    fn test_variables() {
        let mut fc = ForthCompiler::new();

        fc.execute_string("variable abc", GasLimit::Limited(100))
            .unwrap();

        fc.execute_string("123 abc !", GasLimit::Limited(100))
            .unwrap();
        assert_eq!(fc.sm.st.data_stack.len(), 0);

        fc.execute_string("abc @", GasLimit::Limited(100)).unwrap();
        assert_eq!(fc.sm.st.data_stack, vec![123]);
    }

    #[test]
    fn test_execute_intrinsics_1() {
        let mut fc = ForthCompiler::new();

        fc.execute_string("123 321 + 2 *", GasLimit::Limited(100))
            .unwrap();

        assert_eq!(&fc.sm.st.data_stack, &vec![888]);

        fc.execute_string("123 321 + 2 *", GasLimit::Limited(100))
            .unwrap();

        assert_eq!(&fc.sm.st.data_stack, &vec![888, 888]);
    }

    #[test]
    fn test_compile_1() {
        let mut fc = ForthCompiler::new();

        fc.execute_string(
            ": RickTest 123 321 + 2 * ; RickTest",
            GasLimit::Limited(100),
        )
        .unwrap();

        assert_eq!(&fc.sm.st.data_stack, &vec![888]);

        fc.execute_string("123 321 + 2 * RickTest", GasLimit::Limited(100))
            .unwrap();

        assert_eq!(&fc.sm.st.data_stack, &vec![888, 888, 888]);
    }

    #[test]
    fn test_compile_2() {
        let mut fc = ForthCompiler::new();

        fc.execute_string(
            ": RickTest 123 321 + 2 * ; RickTest : RickTestB 123 321 + 2 * ;",
            GasLimit::Limited(100),
        )
        .unwrap();

        assert_eq!(&fc.sm.st.data_stack, &vec![888]);

        fc.execute_string("123 321 + 2 * RickTest", GasLimit::Limited(100))
            .unwrap();

        assert_eq!(&fc.sm.st.data_stack, &vec![888, 888, 888]);
    }

    #[test]
    fn test_compile_3() {
        let mut fc = ForthCompiler::new();

        fc.execute_string(
            "2 2 - DROP : RickTest 123 321 + 2 * ; RickTest : RickTestB 123 321 + 2 * ; 3 3 - DROP",
            GasLimit::Limited(100),
        )
        .unwrap();

        assert_eq!(&fc.sm.st.data_stack, &vec![888]);

        fc.execute_string("123 321 + 2 * RickTest", GasLimit::Limited(100))
            .unwrap();

        assert_eq!(&fc.sm.st.data_stack, &vec![888, 888, 888]);
    }

    #[test]
    fn test_compile_4() {
        let mut fc = ForthCompiler::new();

        fc.execute_string(
            "2 2 - DROP : RickTest 123 321 + 2 * ; : RickTestB 123 321 + 2 * ; 3 3 -",
            GasLimit::Limited(100),
        )
        .unwrap();

        assert_eq!(&fc.sm.st.data_stack, &vec![0]);

        fc.execute_string("123 321 + 2 * RickTest", GasLimit::Limited(100))
            .unwrap();

        assert_eq!(&fc.sm.st.data_stack, &vec![0, 888, 888]);
    }

    #[test]
    fn test_compile_fail_1() {
        let mut fc = ForthCompiler::new();

        match fc.execute_string(
            "2 2 - DROP : RickTest 123 321 + 2 * ; : : RickTestB 123 321 + 2 * ; 3 3 -",
            GasLimit::Limited(100),
        ) {
            Err(ForthError::UnknownToken(ref x)) if x == "RickTestB" => (),
            r => panic!("Incorrect error type returned {:?}", r),
        }
    }

    #[test]
    fn test_compile_fail_2() {
        let mut fc = ForthCompiler::new();

        match fc.execute_string(
            "2 2 - DROP : RickTest 123 321 + 2 * ; ; : RickTestB 123 321 + 2 * ; 3 3 -",
            GasLimit::Limited(100),
        ) {
            Err(ForthError::InvalidSyntax(_)) => (),
            r => panic!("Incorrect error type returned {:?}", r),
        }
    }

    #[test]
    fn test_compile_fail_3() {
        let mut fc = ForthCompiler::new();

        match fc.execute_string(
            "2 2 - DROP : RickTest 123 321 + 2 * ; : RickTestB 123 321 + 2 * ; : ERROR 3 3 -",
            GasLimit::Limited(100),
        ) {
            Err(ForthError::MissingSemicolonAfterColon) => (),
            r => panic!("Incorrect error type returned {:?}", r),
        }
    }

    #[test]
    fn test_compile_syntax() {
        let mut fc = ForthCompiler::new();

        match fc.execute_string(": ", GasLimit::Limited(100)) {
            Err(ForthError::InvalidSyntax(_)) => (),
            r => panic!("Incorrect error type returned {:?}", r),
        }

        // todo
        match fc.execute_string(": myadd 2 3 : ", GasLimit::Limited(100)) {
            Err(ForthError::InvalidSyntax(_)) => (), //returning wrong error message..
            r => panic!("Incorrect error type returned {:?}", r),
        }
    }

    #[test]
    fn test_variable_syntax() {
        let mut fc = ForthCompiler::new();

        match fc.execute_string("variable ", GasLimit::Limited(100)) {
            Err(ForthError::InvalidSyntax(_)) => (),
            r => panic!("Incorrect error type returned {:?}", r),
        }
    }

    #[test]
    fn test_constant_syntax() {
        let mut fc = ForthCompiler::new();

        match fc.execute_string("constant ", GasLimit::Limited(100)) {
            Err(ForthError::InvalidSyntax(_)) => (),
            r => panic!("Incorrect error type returned {:?}", r),
        }

        fc.execute_string("69 constant abc", GasLimit::Limited(100))
            .unwrap();

        match fc.execute_string("constant abc ", GasLimit::Limited(100)) {
            Err(ForthError::ConstantAlreadyExists(_)) => (),
            r => panic!("Incorrect error type returned {:?}", r),
        }
    }

    #[test]
    fn test_if_syntax() {
        let mut fc = ForthCompiler::new();

        // todo!
        match fc.execute_string(": myfunc  1 2 IF ; ", GasLimit::Limited(100)) {
            Err(ForthError::InvalidSyntax(_)) => (), // should return an error..
            r => panic!("Incorrect error type returned {:?}", r),
        }

        //todo
        match fc.execute_string(": myfunc  1 2 if 1 + else ; ", GasLimit::Limited(100)) {
            Err(ForthError::InvalidSyntax(_)) => (), // should return an error
            r => panic!("Incorrect error type returned {:?}", r),
        }

        //todo
        match fc.execute_string(": myfunc  1 2 else then ; ", GasLimit::Limited(100)) {
            Err(ForthError::InvalidSyntax(_)) => (),
            r => panic!("Incorrect error type returned {:?}", r),
        }
        //todo
        match fc.execute_string(": myfunc  1 2 then ; ", GasLimit::Limited(100)) {
            Err(ForthError::InvalidSyntax(_)) => (),
            r => panic!("Incorrect error type returned {:?}", r),
        }
    }

    #[test]
    fn test_print() {
        let mut fc = ForthCompiler::new();

        assert_eq!(
            fc.execute_string("42 .", GasLimit::Limited(100)).unwrap(),
            "42 "
        );

        assert_eq!(&fc.sm.st.data_stack, &vec![]);
    }

    #[test]
    fn test_if1() {
        let mut fc = ForthCompiler::new();

        fc.execute_string("0 IF 1 2 + THEN", GasLimit::Limited(100))
            .unwrap();

        assert_eq!(&fc.sm.st.data_stack, &vec![]);
    }

    #[test]
    fn test_if2() {
        let mut fc = ForthCompiler::new();

        fc.execute_string("1 IF 1 2 + THEN", GasLimit::Limited(100))
            .unwrap();

        assert_eq!(&fc.sm.st.data_stack, &vec![3]);
    }

    #[test]
    fn test_if_else_1() {
        let mut fc = ForthCompiler::new();

        fc.execute_string("0 IF 1 2 + ELSE 3 4 + THEN", GasLimit::Limited(100))
            .unwrap();

        assert_eq!(&fc.sm.st.data_stack, &vec![7]);
    }

    #[test]
    fn test_if_else_2() {
        let mut fc = ForthCompiler::new();

        fc.execute_string("1 IF 1 2 + ELSE 3 4 + THEN", GasLimit::Limited(100))
            .unwrap();

        assert_eq!(&fc.sm.st.data_stack, &vec![3]);
    }

    #[test]
    fn test_if_else_3() {
        let mut fc = ForthCompiler::new();

        fc.execute_string("-1 IF 1 2 + ELSE 3 4 + THEN", GasLimit::Limited(100))
            .unwrap();

        assert_eq!(&fc.sm.st.data_stack, &vec![3]);
    }

    #[test]
    fn test_if_else_4() {
        let mut fc = ForthCompiler::new();

        fc.execute_string("1 IF 1 2 + 1 + ELSE 3 4 + THEN", GasLimit::Limited(100))
            .unwrap();

        assert_eq!(&fc.sm.st.data_stack, &vec![4]);
    }

    #[test]
    fn test_if_else_5() {
        let mut fc = ForthCompiler::new();

        fc.execute_string("0 IF 1 2 + 1 + ELSE 3 4 + 1 + THEN", GasLimit::Limited(100))
            .unwrap();

        assert_eq!(&fc.sm.st.data_stack, &vec![8]);
    }

    #[test]
    fn test_if_else_6() {
        // emulates MAX
        let mut fc = ForthCompiler::new();

        fc.execute_string(
            "10 20 DUP DUP > IF DROP + ELSE SWAP DROP THEN",
            GasLimit::Limited(100),
        )
        .unwrap();

        assert_eq!(&fc.sm.st.data_stack, &vec![20]);
    }

    #[test]
    fn test_trap_1() {
        let mut fc = ForthCompiler::new();

        // Simulate a IO OUT command, at TRAP(100)
        fc.sm
            .trap_handlers
            .push(Box::from(TrapHandler::new(100, |_trap_id, st| {
                let io_port = st
                    .data_stack
                    .pop()
                    .ok_or(StackMachineError::NumberStackUnderflow)?;
                let io_value = st
                    .data_stack
                    .pop()
                    .ok_or(StackMachineError::NumberStackUnderflow)?;
                println!(
                    "Simulated IO OUT command to Port: {} and Value: {}",
                    io_port, io_value
                );
                Ok(TrapHandled::Handled)
            })));

        fc.execute_string(
            ": IO_OUT 100 TRAP ; 1234 1000 IO_OUT",
            GasLimit::Limited(100),
        )
        .unwrap();

        // Nothing left over
        assert_eq!(&fc.sm.st.data_stack, &vec![]);
    }

    #[test]
    fn test_trap_2() {
        let mut fc = ForthCompiler::new();

        // Simulate a IO IN command, at TRAP(101)
        fc.sm
            .trap_handlers
            .push(Box::from(TrapHandler::new(101, |_trap_id, st| {
                let io_port = st
                    .data_stack
                    .pop()
                    .ok_or(StackMachineError::NumberStackUnderflow)?;
                let io_value = 6543;
                println!(
                    "Simulated IO IN command from Port: {} and Value: {}",
                    io_port, io_value
                );
                st.data_stack.push(io_value);
                Ok(TrapHandled::Handled)
            })));

        fc.execute_string(": IO_IN 101 TRAP ; 1000 IO_IN", GasLimit::Limited(100))
            .unwrap();

        // Value from IO port on stack
        assert_eq!(&fc.sm.st.data_stack, &vec![6543]);
    }

    #[test]
    fn test_trap_3() {
        let mut fc = ForthCompiler::new();

        // Simulate a IO OUT command, at TRAP(100), but define the port number inside a Forth Word as well
        fc.sm
            .trap_handlers
            .push(Box::from(TrapHandler::new(100, |_trap_id, st| {
                let io_port = st
                    .data_stack
                    .pop()
                    .ok_or(StackMachineError::NumberStackUnderflow)?;
                let io_value = st
                    .data_stack
                    .pop()
                    .ok_or(StackMachineError::NumberStackUnderflow)?;
                println!(
                    "Simulated IO OUT command to Port: {} and Value: {}",
                    io_port, io_value
                );
                Ok(TrapHandled::Handled)
            })));

        fc.execute_string(
            ": IO_OUT 100 TRAP ; : OUT_DISPLAY 1000 IO_OUT ; 1234 OUT_DISPLAY",
            GasLimit::Limited(100),
        )
        .unwrap();

        // Nothing left over
        assert_eq!(&fc.sm.st.data_stack, &vec![]);
    }

    #[test]
    fn test_trap_4() {
        let mut fc = ForthCompiler::new();

        // Simulate a IO IN command, at TRAP(101), but define the port number inside a Forth word as well
        fc.sm
            .trap_handlers
            .push(Box::from(TrapHandler::new(101, |_trap_id, st| {
                let io_port = st
                    .data_stack
                    .pop()
                    .ok_or(StackMachineError::NumberStackUnderflow)?;
                let io_value = 6543;
                println!(
                    "Simulated IO IN command from Port: {} and Value: {}",
                    io_port, io_value
                );
                st.data_stack.push(io_value);
                Ok(TrapHandled::Handled)
            })));

        fc.execute_string(
            ": IO_IN 101 TRAP ; : IN_KEYBOARD 1000 IO_IN ; IN_KEYBOARD",
            GasLimit::Limited(100),
        )
        .unwrap();

        // Value from IO port on stack
        assert_eq!(&fc.sm.st.data_stack, &vec![6543]);
    }
}
