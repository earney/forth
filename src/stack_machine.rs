//use std::convert::TryFrom;
use super::memory::Memory;
use std::collections::HashMap;
//use std::io::Read;
//use std::fmt::Write;

use console::Term;

type StackType = i16;
type DWType = i32; // double widith type,  used by */ to do more accurate math.

pub enum GasLimit {
    Unlimited,
    Limited(usize),
}

#[derive(Debug)]
pub enum StackMachineError {
    UnkownError,
    NumberStackUnderflow,
    UnhandledTrap,
    RanOutOfGas,
}

pub enum TrapHandled {
    Handled,
    NotHandled,
}

// Chain of Command Pattern
pub trait HandleTrap {
    fn handle_trap(
        &mut self,
        trap_id: StackType,
        st: &mut StackMachineState,
    ) -> Result<TrapHandled, StackMachineError>;
}

pub struct TrapHandler<'a> {
    handled_trap: StackType,
    to_run: Box<
        dyn Fn(StackType, &mut StackMachineState) -> Result<TrapHandled, StackMachineError> + 'a,
    >,
}

impl<'a> TrapHandler<'a> {
    pub fn new<C>(handled_trap: StackType, f: C) -> TrapHandler<'a>
    where
        C: Fn(StackType, &mut StackMachineState) -> Result<TrapHandled, StackMachineError> + 'a,
    {
        TrapHandler {
            handled_trap,
            to_run: Box::new(f),
        }
    }
}

impl<'a> HandleTrap for TrapHandler<'a> {
    fn handle_trap(
        &mut self,
        trap_number: StackType,
        st: &mut StackMachineState,
    ) -> Result<TrapHandled, StackMachineError> {
        if trap_number == self.handled_trap {
            return (self.to_run)(self.handled_trap, st);
        }
        Ok(TrapHandled::NotHandled)
    }
}

#[derive(Debug, Clone)]
pub enum Opcode {
    // redirect flow
    JMP,
    JR,
    JRZ,
    JRNZ,
    CALL,

    // comparison ops
    CMPZ,
    CMPNZ,
    EQ,
    LT,
    GT,

    // load integer
    LDI(StackType),

    // stack ops
    DROP,
    SWAP,
    TWODROP,
    TWOSWAP,
    RET,
    DUP,
    TWODUP,
    QuestionDup,
    TRAP,
    OVER,
    TWOOVER,
    TWOROT,
    ROT,
    ToR,
    RFrom,
    RFetch,
    DEPTH,

    //math ops
    ADD,
    SUB,
    MUL,
    DIV,
    MOD,
    DivMOD,
    MultDivMOD,
    STARSLASH, //  */ does multiply then divide using DWType (Double Width Type)
    NEGATE,
    OnePlus,
    OneMinus,
    TwoPlus,
    TwoMinus,
    LShift,
    RShift,

    // logic operators
    NOT,
    AND,
    OR,
    XOR,
    ZeroLess,
    ZeroEqual,
    ZeroGreater,

    // misc
    NOP,

    //variable
    // VARIABLE(StackType), // used to put variable +resses on the stack.
    BANG,
    AT,
    Comma,
    // GETCONSTANT(String),
    SETCONSTANT(String),
    CREATE(String),
    ALLOCATE,
    CELLS,
    CHARS,
    CHARPLUS,
    CBang,
    CMOVE,

    // io
    DUMP,
    PRINT,
    EMIT,
    CHAR(char),
    KEY,
    PRINTTEXT(String),
    SPACE,
    SPACES,
    CR,
    QuestionMark,
    RightAlign,
    COUNT,
    TYPE,
    FormattedNumericOutput(String),

    //chars and strings
    SDoubleTick(String),

    // memory opcodes
    CAt, // C@

    //random
    SETSEED,
    RAND,
    DEBUG(String),
}

pub struct StackMachineState {
    pub data_stack: Vec<StackType>,
    pub return_stack: Vec<StackType>,
    pub opcodes: Vec<Opcode>,
    pub variable_addresses: HashMap<String, StackType>,
    pub variables: Vec<StackType>,
    pub constants: HashMap<String, StackType>,
    pub text: Vec<String>,
    pub memory: Memory,
    // pub loop_stack: Vec<StackType>,
    pc: usize,
    gas_used: usize,
    random_seed: usize,
    debug: bool,
}

impl Default for StackMachineState {
    fn default() -> Self {
        Self::new()
    }
}

impl StackMachineState {
    pub fn new() -> StackMachineState {
        StackMachineState {
            data_stack: Vec::new(),
            return_stack: Vec::new(),
            opcodes: Vec::new(),
            variable_addresses: HashMap::new(),
            variables: Vec::new(),
            constants: HashMap::new(),
            text: Vec::new(),
            memory: Memory::new(),
            // loop_stack: Vec::new(),
            pc: 0,
            gas_used: 0,
            random_seed: 7,
            debug: false,
        }
    }
}

impl StackMachineState {
    pub fn gas_used(&self) -> usize {
        self.gas_used
    }
}

pub struct StackMachine {
    pub st: StackMachineState,
    pub trap_handlers: Vec<Box<dyn HandleTrap>>,
}

impl Default for StackMachine {
    fn default() -> Self {
        Self::new()
    }
}

impl StackMachine {
    pub fn new() -> StackMachine {
        StackMachine {
            st: StackMachineState::new(),
            trap_handlers: Vec::new(),
        }
    }

    pub fn execute(
        &mut self,
        starting_point: usize,
        gas_limit: GasLimit,
    ) -> Result<String, StackMachineError> {
        self.st.gas_used = 0;
        self.st.pc = starting_point;

        let mut output: String = String::from("");

        macro_rules! NS_POP {
            () => {
                self.st
                    .data_stack
                    .pop()
                    .ok_or(StackMachineError::NumberStackUnderflow)?
            };
        }

        if self.st.debug {
            println!("{:?}", self.st.opcodes);
        }
        loop {
            if self.st.debug {
                println!("{}: opcode:{:?}", self.st.pc, self.st.opcodes[self.st.pc]);
            }
            let mut pc_reset = false;
            match &self.st.opcodes[self.st.pc] {
                Opcode::PRINT => {
                    let x = NS_POP!();
                    output.push_str(&format!("{} ", x).to_string());
                }
                Opcode::EMIT => {
                    let x: StackType = NS_POP!();
                    match x {
                        0..=127 => output.push(x as u8 as char),
                        _ => todo!(),
                    }
                }
                Opcode::CHAR(c) => match c.is_ascii() {
                    true => self.st.data_stack.push(*c as u8 as StackType),
                    false => todo!(),
                },
                Opcode::PRINTTEXT(s) => {
                    //   println!("{}", s);
                    output.push_str(s);
                }
                Opcode::FormattedNumericOutput(_s) => {
                    // todo
                }
                Opcode::SPACE => {
                    output.push(' ');
                }
                Opcode::SPACES => {
                    let x = NS_POP!();
                    for _i in 0..x {
                        output.push(' ');
                    }
                }
                Opcode::CR => {
                    output.push('\n');
                }
                Opcode::QuestionMark => {
                    //let var_addr = NS_POP!() as usize;
                    //let value = self.st.memory.get(var_addr);
                    let mem_addr = NS_POP!() as usize;
                    let value = self.st.memory.get_memory_address(mem_addr);
                    //self.st.data_stack.push(value[0]);

                    output.push_str(&format!("{} ", value[0]).to_string());
                }
                Opcode::RightAlign => {
                    let width = NS_POP!() as usize;
                    let value = NS_POP!() as usize;
                    let out = format!("{:width$}", value, width = width);
                    output.push_str(&out);
                }

                Opcode::COUNT => {
                    let caddr1 = NS_POP!() as StackType;
                    self.st.data_stack.push(caddr1 + 1);
                    let len = self.st.memory.cell_type_array[caddr1 as usize].length;
                    self.st.data_stack.push(len as StackType);
                }
                Opcode::DUMP | Opcode::TYPE => {
                    let len = NS_POP!() as usize;
                    let addr = NS_POP!() as usize;
                    let s = &self.st.memory.data[addr..addr + len]; //string
                    output.push_str(&format!("{:?}", s));
                }
                Opcode::SDoubleTick(_s) => {
                    //fix me..
                    /*
                    let addr = self.st.memory.len();
                    let bytes = s.as_str().as_bytes();
                    self.st.memory.push(bytes.len() as u8);
                    for mybyte in bytes.iter().take(bytes.len()) {
                        self.st.memory.push(*mybyte);
                    }

                    self.st.data_stack.push(bytes.len() as StackType);
                    self.st.data_stack.push(addr as StackType);
                    */
                }
                Opcode::JR => {
                    let x: StackType = NS_POP!();
                    self.st.pc = (self.st.pc as StackType + x) as usize;
                    pc_reset = true;
                }
                Opcode::CALL => {
                    self.st.return_stack.push(self.st.pc as StackType + 1);
                    self.st.pc = NS_POP!() as usize;
                    pc_reset = true;
                }
                Opcode::TWOOVER => {
                    let x1 = NS_POP!();
                    let x2 = NS_POP!();
                    let y1 = NS_POP!();
                    let y2 = NS_POP!();
                    self.st.data_stack.push(y2);
                    self.st.data_stack.push(y1);
                    self.st.data_stack.push(x2);
                    self.st.data_stack.push(x1);
                    self.st.data_stack.push(y2);
                    self.st.data_stack.push(y1);
                }
                Opcode::OVER => {
                    let x = NS_POP!();
                    let y = NS_POP!();
                    self.st.data_stack.push(y);
                    self.st.data_stack.push(x);
                    self.st.data_stack.push(y);
                }
                Opcode::ROT => {
                    let x = NS_POP!();
                    let y = NS_POP!();
                    let z = NS_POP!();
                    self.st.data_stack.push(y);
                    self.st.data_stack.push(x);
                    self.st.data_stack.push(z);
                }
                Opcode::TWOROT => {
                    let x1 = NS_POP!();
                    let x2 = NS_POP!();
                    let y1 = NS_POP!();
                    let y2 = NS_POP!();
                    let z1 = NS_POP!();
                    let z2 = NS_POP!();
                    self.st.data_stack.push(y2);
                    self.st.data_stack.push(y1);
                    self.st.data_stack.push(x2);
                    self.st.data_stack.push(x1);
                    self.st.data_stack.push(z2);
                    self.st.data_stack.push(z1);
                }
                Opcode::DEPTH => {
                    self.st
                        .data_stack
                        .push(self.st.data_stack.len() as StackType);
                }
                Opcode::CMPZ => {
                    let x = NS_POP!();
                    if x == 0 {
                        self.st.data_stack.push(0);
                    } else {
                        self.st.data_stack.push(-1);
                    }
                }
                Opcode::CMPNZ => {
                    let x = NS_POP!();
                    if x == 0 {
                        self.st.data_stack.push(-1);
                    } else {
                        self.st.data_stack.push(0);
                    }
                }
                Opcode::EQ => {
                    let x = NS_POP!();
                    let y = NS_POP!();
                    if x == y {
                        self.st.data_stack.push(-1);
                    } else {
                        self.st.data_stack.push(0);
                    }
                }
                Opcode::LT => {
                    let x = NS_POP!();
                    let y = NS_POP!();
                    if self.st.debug {
                        println!("{} < {} = {}", y, x, y < x);
                    }
                    if y < x {
                        self.st.data_stack.push(-1);
                    } else {
                        self.st.data_stack.push(0);
                    }
                }
                Opcode::GT => {
                    let x = NS_POP!();
                    let y = NS_POP!();
                    if self.st.debug {
                        println!("{} > {} = {}", y, x, y > x);
                    }
                    if y > x {
                        self.st.data_stack.push(-1);
                    } else {
                        self.st.data_stack.push(0);
                    }
                }
                Opcode::ZeroLess => {
                    let x = NS_POP!();
                    self.st.data_stack.push(if x < 0 { 1 } else { 0 });
                }
                Opcode::ZeroEqual => {
                    let x = NS_POP!();
                    self.st.data_stack.push(if x == 0 { 1 } else { 0 });
                }
                Opcode::ZeroGreater => {
                    let x = NS_POP!();
                    self.st.data_stack.push(if x > 0 { 1 } else { 0 });
                }
                Opcode::JRZ => {
                    let x = NS_POP!();
                    let y = NS_POP!();
                    if y == 0 {
                        self.st.pc = (self.st.pc as StackType + x) as usize;
                        pc_reset = true;
                    }
                }
                Opcode::JRNZ => {
                    let x = NS_POP!();
                    let y = NS_POP!();
                    if y != 0 {
                        self.st.pc = (self.st.pc as StackType + x) as usize;
                        pc_reset = true;
                    }
                }
                Opcode::JMP => {
                    let x = NS_POP!();
                    self.st.pc = (self.st.pc as StackType + x) as usize;
                    pc_reset = true;
                }
                Opcode::LDI(x) => self.st.data_stack.push(*x),
                Opcode::DROP => {
                    let _x = NS_POP!();
                }
                Opcode::TWODROP => {
                    let _x = NS_POP!();
                    let _x = NS_POP!();
                }
                Opcode::RET => {
                    match self.st.return_stack.pop() {
                        None => return Ok(output),
                        Some(oldpc) => self.st.pc = oldpc as usize,
                    };
                    pc_reset = true;
                }
                Opcode::NEGATE => {
                    let x = NS_POP!();
                    self.st.data_stack.push(-x);
                }
                Opcode::ADD => {
                    let x = NS_POP!();
                    let y = NS_POP!();
                    self.st.data_stack.push(x + y);
                }
                Opcode::SUB => {
                    let x = NS_POP!();
                    let y = NS_POP!();
                    self.st.data_stack.push(y - x);
                }
                Opcode::MUL => {
                    let x = NS_POP!();
                    let y = NS_POP!();
                    self.st.data_stack.push(x * y);
                }
                Opcode::DIV => {
                    let x = NS_POP!();
                    let y = NS_POP!();
                    self.st.data_stack.push(y / x);
                }
                Opcode::MOD => {
                    let x = NS_POP!();
                    let y = NS_POP!();
                    self.st.data_stack.push(y % x);
                }
                Opcode::DivMOD => {
                    let x: DWType = NS_POP!() as DWType;
                    let y: DWType = NS_POP!() as DWType;

                    self.st.data_stack.push((y % x) as StackType);
                    self.st.data_stack.push((y / x) as StackType);
                }
                Opcode::MultDivMOD => {
                    let x: DWType = NS_POP!() as DWType;
                    let y: DWType = NS_POP!() as DWType;
                    let z: DWType = NS_POP!() as DWType;

                    let temp = z * y as DWType;
                    self.st.data_stack.push((temp % x) as StackType);
                    self.st.data_stack.push((temp / x) as StackType);
                }
                Opcode::STARSLASH => {
                    // used Double with so that math is more accurate..
                    let x: DWType = NS_POP!() as DWType;
                    let y: DWType = NS_POP!() as DWType;
                    let z: DWType = NS_POP!() as DWType;

                    let temp = (z * y / x) as StackType;
                    self.st.data_stack.push(temp as StackType);
                }
                Opcode::OnePlus => {
                    let x = NS_POP!();
                    self.st.data_stack.push(x + 1);
                }
                Opcode::OneMinus => {
                    let x = NS_POP!();
                    self.st.data_stack.push(x - 1);
                }
                Opcode::TwoPlus => {
                    let x = NS_POP!();
                    self.st.data_stack.push(x + 2);
                }
                Opcode::TwoMinus => {
                    let x = NS_POP!();
                    self.st.data_stack.push(x - 2);
                }
                Opcode::LShift => {
                    let x = NS_POP!();
                    let y = NS_POP!();
                    self.st.data_stack.push(y << x);
                }
                Opcode::RShift => {
                    let x = NS_POP!();
                    let y = NS_POP!();
                    self.st.data_stack.push(y >> x);
                }
                // logical operators
                Opcode::NOT => {
                    let x = NS_POP!();
                    self.st.data_stack.push(!x);
                }
                Opcode::AND => {
                    let x = NS_POP!();
                    let y = NS_POP!();
                    self.st.data_stack.push(x & y);
                }
                Opcode::OR => {
                    let x = NS_POP!();
                    let y = NS_POP!();
                    self.st.data_stack.push(x | y);
                }
                Opcode::XOR => {
                    let x = NS_POP!();
                    let y = NS_POP!();
                    self.st.data_stack.push(x ^ y);
                }
                Opcode::DUP => {
                    let x = NS_POP!();
                    self.st.data_stack.push(x);
                    self.st.data_stack.push(x);
                }
                Opcode::TWODUP => {
                    let x1 = NS_POP!();
                    let x2 = NS_POP!();
                    self.st.data_stack.push(x2);
                    self.st.data_stack.push(x1);
                    self.st.data_stack.push(x2);
                    self.st.data_stack.push(x1);
                }
                Opcode::QuestionDup => {
                    let x = NS_POP!();
                    self.st.data_stack.push(x);
                    if x != 0 {
                        self.st.data_stack.push(x);
                    }
                }
                Opcode::SWAP => {
                    let x = NS_POP!();
                    let y = NS_POP!();
                    self.st.data_stack.push(x);
                    self.st.data_stack.push(y);
                }
                Opcode::TWOSWAP => {
                    let x1 = NS_POP!();
                    let x2 = NS_POP!();
                    let y1 = NS_POP!();
                    let y2 = NS_POP!();
                    self.st.data_stack.push(x1);
                    self.st.data_stack.push(x2);
                    self.st.data_stack.push(y1);
                    self.st.data_stack.push(y2);
                }
                Opcode::TRAP => {
                    // We are going to say that TRAPs always have a numeric code on the number stack to define which TRAP is being called
                    let trap_id = NS_POP!();
                    for h in self.trap_handlers.iter_mut() {
                        if let TrapHandled::Handled = h.handle_trap(trap_id, &mut self.st)? {
                            return Ok("".to_string());
                        }
                    }
                    return Err(StackMachineError::UnhandledTrap);
                }
                Opcode::ToR => {
                    //takes a value from the parameter( number) stack and push it on the return stack
                    let x = NS_POP!();
                    // println!("TOR {}", x);
                    self.st.return_stack.push(x);
                    //  println!("{:?}", self.st.return_stack);
                }
                Opcode::RFrom => {
                    // takes a value from the return stack and put it on the parameter stack.
                    match self.st.return_stack.pop() {
                        Some(x) => self.st.data_stack.push(x),
                        _ => todo!(),
                    }
                }
                Opcode::RFetch => {
                    // copies the value from the return stack and puts it on the number stack.
                    match self.st.return_stack.pop() {
                        Some(x) => {
                            self.st.data_stack.push(x);
                            self.st.return_stack.push(x);
                        }
                        _ => todo!(),
                    }
                }
                Opcode::CAt => {
                    // (addr -- b )
                    let addr = NS_POP!();
                    let value = self.st.memory.get(addr as usize);
                    self.st.data_stack.push(value[0] as StackType);
                }
                /*
                Opcode::VARIABLE(addr) => {
                    self.st.data_stack.push(*addr);
                }
                */
                  /*
                Opcode::GETCONSTANT(con) => {
                    match self.st.constants.get(con) {
                        Some(x) => self.st.data_stack.push(*x),
                        None => todo!(), //Err(ForthError::ConstantDoesNotExist(con.to_string())),
                    }
                }
                */
                Opcode::SETCONSTANT(con) => {
                    let x = NS_POP!();
                    match self.st.memory.get_word_addr(con.to_string()) {
                        Some(word_addr) => {
                            self.st.memory.allocate(word_addr, 0);
                            self.st.memory.set(vec![x], word_addr);
                        }
                        _ => todo!(),
                    }
                }
                Opcode::CREATE(word) => match self.st.memory.get_word_addr(word.to_string()) {
                    Some(word_addr) => {
                        self.st.memory.allocate(word_addr, 0);
                    } // assume one cell for now.
                    _ => todo!(),
                },
                Opcode::CHARS => { // ( n1 -- n2 )
                     //  let units = NS_POP!();
                     //  self.st.data_stack.push(units * 1);
                }
                Opcode::CMOVE => {
                    let len = NS_POP!() as usize;
                    let addr2 = NS_POP!() as usize;
                    let addr1 = NS_POP!() as usize;
                    if len > 0 {
                        for i in 0..len {
                            self.st.memory.data[addr2 + i] = self.st.memory.data[addr1 + i];
                        }
                    }
                }
                Opcode::CBang => {
                    let addr = NS_POP!() as usize;
                    let chr = NS_POP!() as u8;
                    self.st.memory.data[addr] = chr;
                }
                Opcode::CHARPLUS => {
                    let addr = NS_POP!();
                    self.st.data_stack.push(addr + 1);
                }
                Opcode::CELLS => {
                    // ( n1 -- n2 )
                    let units = NS_POP!();
                    self.st.data_stack.push(units * 2);
                }
                Opcode::ALLOCATE => {
                    let amount = NS_POP!();
                    match amount > 0 {
                        true => {
                            let word_addr = match self.st.memory.last_created_word_addr {
                                Some(x) => x,
                                None => todo!(),
                            };
                            self.st.memory.allocate(word_addr, amount as usize);
                            self.st.memory.last_created_word_addr = None;
                        }
                        false => {
                            let word_addr = match self.st.memory.last_created_word_addr {
                                Some(x) => x,
                                None => todo!(),
                            };
                            let current_size =
                                self.st.memory.cell_type_array[word_addr].length as StackType;
                            self.st
                                .memory
                                .allocate(word_addr, (current_size + amount) as usize);
                            self.st.memory.last_created_word_addr = None;
                        }
                    }
                }
                Opcode::BANG => {
                    let mem_addr = NS_POP!() as usize;
                    let value = NS_POP!();
                    self.st.memory.set_memory_address(vec![value], mem_addr);
                    // let word_addr = match self.st.memory.last_created_word_addr {
                    //     Some(x) => x,
                    //     None => todo!(),
                    // };
                    // self.st.memory.cell_type_array[word_addr].length+=2;
                    //self.st.variables[var_addr] = value;
                }
                Opcode::AT => {
                    let mem_addr = NS_POP!() as usize;
                    let value = self.st.memory.get_memory_address(mem_addr);
                    self.st.data_stack.push(value[0]);
                }
                Opcode::Comma => {
                    let value = NS_POP!();
                    self.st.memory.allocate_byte_and_set_value(value);
                }
                Opcode::NOP => {}
                // sets random seed
                Opcode::SETSEED => {
                    self.st.random_seed = NS_POP!() as usize;
                }
                // pushes a random number on the stack.
                Opcode::RAND => {
                    self.st.random_seed ^= self.st.random_seed << 13;
                    self.st.random_seed ^= self.st.random_seed >> 17;
                    self.st.random_seed ^= self.st.random_seed << 5;
                    self.st.data_stack.push(self.st.random_seed as StackType)
                }
                Opcode::KEY => {
                    let t = Term::stdout();
                    match t.read_char() {
                        Ok(c) => {
                            print!("{}", c);
                            self.st.data_stack.push(c as StackType)
                        }
                        Err(c) => panic!("{}", c),
                    };
                }
                Opcode::DEBUG(_s) => {} // ignore.. this is for debugging the stack.
            }
            if !pc_reset {
                self.st.pc += 1;
            }

            self.st.gas_used += 1;

            //if self.st.debug {
            //    println!("after data stack: {:?}", self.st.data_stack);
            //    println!("after return stack: {:?}", self.st.return_stack);
            //    println!("================");
            //}

            if let GasLimit::Limited(x) = gas_limit {
                if self.st.gas_used > x {
                    return Err(StackMachineError::RanOutOfGas);
                }
            }
        }
        //Ok(format!("{} ok.", output))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_char() {
        let mut sm = StackMachine::new();
        sm.st
            .opcodes
            .extend_from_slice(&[Opcode::CHAR('a'), Opcode::RET]);

        sm.execute(0, GasLimit::Limited(100)).unwrap();
        assert_eq!(sm.st.data_stack, vec![97]);
    }

    #[test]
    fn test_rshift() {
        let mut sm = StackMachine::new();

        sm.st.data_stack.extend_from_slice(&[4, 1]);
        // Put the opcodes into the *memory*
        sm.st
            .opcodes
            .extend_from_slice(&[Opcode::RShift, Opcode::RET]);

        sm.execute(0, GasLimit::Limited(100)).unwrap();
        assert_eq!(sm.st.data_stack, vec![2]);

        sm.st.data_stack.clear();
        sm.st.data_stack.extend_from_slice(&[16, 2]);
        // Put the opcodes into the *memory*
        sm.st
            .opcodes
            .extend_from_slice(&[Opcode::RShift, Opcode::RET]);

        sm.execute(0, GasLimit::Limited(100)).unwrap();
        assert_eq!(sm.st.data_stack, vec![4]);
    }

    #[test]
    fn test_lshift() {
        let mut sm = StackMachine::new();

        sm.st.data_stack.extend_from_slice(&[4, 1]);
        // Put the opcodes into the *memory*
        sm.st
            .opcodes
            .extend_from_slice(&[Opcode::LShift, Opcode::RET]);

        sm.execute(0, GasLimit::Limited(100)).unwrap();
        assert_eq!(sm.st.data_stack, vec![8]);

        sm.st.data_stack.clear();
        sm.st.data_stack.extend_from_slice(&[8, 2]);
        // Put the opcodes into the *memory*
        sm.st
            .opcodes
            .extend_from_slice(&[Opcode::LShift, Opcode::RET]);

        sm.execute(0, GasLimit::Limited(100)).unwrap();
        assert_eq!(sm.st.data_stack, vec![32]);
    }

    #[test]
    fn test_tor() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[1]);
        //sm.st.return_stack.extend_from_slice(&[0]);

        sm.st.opcodes.extend_from_slice(&[Opcode::ToR, Opcode::RET]);

        sm.execute(0, GasLimit::Limited(100)).unwrap();
        assert_eq!(sm.st.data_stack.len(), 0);
        //  assert_eq!(sm.st.return_stack, vec![1]);    value on return stack is removed with the RET code
    }

    #[test]
    fn test_rfrom() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.return_stack.extend_from_slice(&[1]);

        sm.st
            .opcodes
            .extend_from_slice(&[Opcode::RFrom, Opcode::RET]);

        sm.execute(0, GasLimit::Limited(100)).unwrap();
        assert_eq!(sm.st.data_stack, vec![1]);
        assert!(sm.st.return_stack.is_empty());
    }

    #[test]
    fn test_rfetch() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.return_stack.extend_from_slice(&[1]);

        sm.st
            .opcodes
            .extend_from_slice(&[Opcode::RFetch, Opcode::RET]);

        sm.execute(0, GasLimit::Limited(100)).unwrap();
        assert_eq!(sm.st.data_stack, vec![1]);
        // assert_eq!(sm.st.return_stack, vec![1]);  // RET will remove the 1 from the return stack
    }

    #[test]
    fn test_print() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[1]);
        // Put the opcodes into the *memory*
        sm.st
            .opcodes
            .extend_from_slice(&[Opcode::PRINT, Opcode::RET]);

        assert_eq!(sm.execute(0, GasLimit::Limited(100)).unwrap(), "1 ");
        assert_eq!(sm.st.data_stack.len(), 0);
    }

    #[test]
    fn test_negate() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[1]);
        // Put the opcodes into the *memory*
        sm.st
            .opcodes
            .extend_from_slice(&[Opcode::NEGATE, Opcode::RET]);

        sm.execute(0, GasLimit::Limited(100)).unwrap();
        assert_eq!(sm.st.data_stack, vec![-1]);
    }
    #[test]
    fn test_rot() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[1, 2, 3]);
        // Put the opcodes into the *memory*
        sm.st.opcodes.extend_from_slice(&[Opcode::ROT, Opcode::RET]);

        sm.execute(0, GasLimit::Limited(100)).unwrap();
        assert_eq!(sm.st.data_stack, vec![2, 3, 1]);
        assert!(sm.st.gas_used() > 0);
    }

    #[test]
    fn test_over() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[1, 2, 3]);
        // Put the opcodes into the *memory*
        sm.st
            .opcodes
            .extend_from_slice(&[Opcode::OVER, Opcode::RET]);

        sm.execute(0, GasLimit::Limited(100)).unwrap();
        assert_eq!(sm.st.data_stack, vec![1, 2, 3, 2]);
    }

    #[test]
    fn test_execute_jr_forward() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[321, 394]);
        // Put the opcodes into the *memory*
        sm.st.opcodes.extend_from_slice(&[
            Opcode::LDI(0),
            Opcode::LDI(1),
            Opcode::LDI(2),
            Opcode::LDI(2), // Jump to location 6 with the JR statement, relative jump of 1
            Opcode::JR,
            Opcode::LDI(3),
            Opcode::LDI(4),
            Opcode::LDI(5),
            Opcode::RET,
        ]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![321, 394, 0, 1, 2, 4, 5]);
    }

    #[test]
    fn test_execute_jr_backward() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[321, 394]);
        // Put the opcodes into the *memory*
        sm.st.opcodes.extend_from_slice(&[
            Opcode::LDI(0),
            Opcode::LDI(1),
            Opcode::RET,
            Opcode::LDI(2),
            Opcode::LDI(-5), // Jump to the LDI(0)
            Opcode::JR,
        ]);

        // Execute the instructions
        sm.execute(3, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![321, 394, 2, 0, 1]);
    }

    #[test]
    fn test_execute_jrz_forward() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[321, 394]);
        // Put the opcodes into the *memory*
        sm.st.opcodes.extend_from_slice(&[
            Opcode::LDI(0),
            Opcode::LDI(1),
            Opcode::LDI(2),
            Opcode::LDI(1), // This won't happen because TOS won't be zero...
            Opcode::LDI(2), // TOS for JRZ
            Opcode::JRZ,
            Opcode::LDI(3),
            Opcode::LDI(4),
            Opcode::LDI(5),
            Opcode::LDI(0),
            Opcode::LDI(2), // Relative Jump of 1
            Opcode::JRZ,    // Jump over the LDI(6)
            Opcode::LDI(6),
            Opcode::LDI(7),
            Opcode::LDI(8),
            Opcode::RET,
        ]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![321, 394, 0, 1, 2, 3, 4, 5, 7, 8]);
    }

    #[test]
    fn test_execute_jrz_backward() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[321, 394]);
        // Put the opcodes into the *memory*
        sm.st.opcodes.extend_from_slice(&[
            Opcode::LDI(0),
            Opcode::RET,
            Opcode::LDI(1),
            Opcode::LDI(2),
            Opcode::LDI(1),  // This won't happen because TOS won't be zero...
            Opcode::LDI(-2), // TOS for JRZ
            Opcode::JRZ,
            Opcode::LDI(3),
            Opcode::LDI(4),
            Opcode::LDI(5),
            Opcode::LDI(0),
            Opcode::LDI(-12), // Relative Jump to start of code
            Opcode::JRZ,      // Jump over the LDI(6)
            Opcode::LDI(6),
            Opcode::LDI(7),
            Opcode::LDI(8),
            Opcode::RET,
        ]);

        // Execute the instructions
        sm.execute(2, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![321, 394, 1, 2, 3, 4, 5, 0]);
    }

    #[test]
    fn test_execute_jrnz_forward() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[321, 394]);
        // Put the opcodes into the *memory*
        sm.st.opcodes.extend_from_slice(&[
            Opcode::LDI(0),
            Opcode::LDI(1),
            Opcode::LDI(2),
            Opcode::LDI(0), // This won't happen because TOS is zero...
            Opcode::LDI(2), // TOS for JRZ
            Opcode::JRNZ,
            Opcode::LDI(3),
            Opcode::LDI(4),
            Opcode::LDI(5),
            Opcode::LDI(1),
            Opcode::LDI(2), // Relative Jump of 1
            Opcode::JRNZ,   // Jump over the LDI(6)
            Opcode::LDI(6),
            Opcode::LDI(7),
            Opcode::LDI(8),
            Opcode::RET,
        ]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![321, 394, 0, 1, 2, 3, 4, 5, 7, 8]);
    }

    #[test]
    fn test_execute_jrnz_backward() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[321, 394]);
        // Put the opcodes into the *memory*
        sm.st.opcodes.extend_from_slice(&[
            Opcode::LDI(0),
            Opcode::RET,
            Opcode::LDI(1),
            Opcode::LDI(2),
            Opcode::LDI(0),  // This won't happen because TOS is zero...
            Opcode::LDI(-2), // TOS for JRZ
            Opcode::JRNZ,
            Opcode::LDI(3),
            Opcode::LDI(4),
            Opcode::LDI(5),
            Opcode::LDI(1),
            Opcode::LDI(-12), // Relative Jump to start of code
            Opcode::JRNZ,     // Jump over the LDI(6)
            Opcode::LDI(6),
            Opcode::LDI(7),
            Opcode::LDI(8),
            Opcode::RET,
        ]);

        // Execute the instructions
        sm.execute(2, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![321, 394, 1, 2, 3, 4, 5, 0]);
    }

    #[test]
    fn test_execute_cmpz_1() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[123, 321, 0]);
        // Put the opcodes into the *memory*
        sm.st
            .opcodes
            .extend_from_slice(&[Opcode::CMPZ, Opcode::RET]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![123, 321, 0]);
    }

    #[test]
    fn test_execute_cmpz_2() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[123, 321, 1]);
        // Put the opcodes into the *memory*
        sm.st
            .opcodes
            .extend_from_slice(&[Opcode::CMPZ, Opcode::RET]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![123, 321, -1]);
    }

    #[test]
    fn test_execute_cmpnz_1() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[123, 321, 0]);
        // Put the opcodes into the *memory*
        sm.st
            .opcodes
            .extend_from_slice(&[Opcode::CMPNZ, Opcode::RET]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![123, 321, -1]);
    }

    #[test]
    fn test_execute_cmpnz_2() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[123, 321, 1]);
        // Put the opcodes into the *memory*
        sm.st
            .opcodes
            .extend_from_slice(&[Opcode::CMPNZ, Opcode::RET]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![123, 321, 0]);
    }

    #[test]
    fn test_execute_eq() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[123, 321]);
        // Put the opcodes into the *memory*
        sm.st.opcodes.extend_from_slice(&[Opcode::EQ, Opcode::RET]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![0]);

        // 0 = 0 => true (-1)
        sm.st.data_stack.extend_from_slice(&[0]);
        sm.st.opcodes.extend_from_slice(&[Opcode::EQ, Opcode::RET]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();
        assert_eq!(sm.st.data_stack, vec![-1]);
    }

    #[test]
    fn test_execute_lt() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[123, 321]);
        // Put the opcodes into the *memory*
        sm.st.opcodes.extend_from_slice(&[Opcode::LT, Opcode::RET]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![-1]);

        // 0 = 0 => true (-1)
        sm.st.data_stack.extend_from_slice(&[-1]);
        sm.st.opcodes.extend_from_slice(&[Opcode::EQ, Opcode::RET]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();
        assert_eq!(sm.st.data_stack, vec![0]);
    }

    #[test]
    fn test_execute_gt() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[123, 321]);
        // Put the opcodes into the *memory*
        sm.st.opcodes.extend_from_slice(&[Opcode::GT, Opcode::RET]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![0]);

        // 0 = 0 => true (-1)
        sm.st.data_stack.extend_from_slice(&[-2]);
        sm.st.opcodes.extend_from_slice(&[Opcode::EQ, Opcode::RET]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();
        assert_eq!(sm.st.data_stack, vec![-1]);
    }

    #[test]
    fn test_execute_call() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[321, 394]);
        // Put the opcodes into the *memory*
        sm.st.opcodes.extend_from_slice(&[
            Opcode::LDI(0),
            Opcode::LDI(5),
            Opcode::CALL,
            Opcode::LDI(1),
            Opcode::RET,
            Opcode::LDI(2),
            Opcode::LDI(10),
            Opcode::CALL,
            Opcode::LDI(3),
            Opcode::RET,
            Opcode::LDI(4),
            Opcode::LDI(15),
            Opcode::CALL,
            Opcode::LDI(5),
            Opcode::RET,
            Opcode::LDI(6),
            Opcode::LDI(20),
            Opcode::CALL,
            Opcode::LDI(7),
            Opcode::RET,
            Opcode::LDI(8),
            Opcode::LDI(25),
            Opcode::CALL,
            Opcode::LDI(9),
            Opcode::RET,
            Opcode::RET,
        ]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(
            sm.st.data_stack,
            vec![321, 394, 0, 2, 4, 6, 8, 9, 7, 5, 3, 1]
        );
    }

    #[test]
    fn test_emit() {
        let mut sm = StackMachine::new();

        sm.st.data_stack.extend_from_slice(&[65, 66]);
        // Put the opcodes into the *memory*
        sm.st
            .opcodes
            .extend_from_slice(&[Opcode::EMIT, Opcode::EMIT, Opcode::RET]);

        // AB should be returned by execute
        assert_eq!(sm.execute(0, GasLimit::Limited(100)).unwrap(), "BA");
        // now just assume that the output is printed..
        // maybe change how this program caches and displays output?
        assert_eq!(sm.st.data_stack, vec![]);
    }

    #[test]
    fn test_execute_ldi() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[321, 394]);
        // Put the opcodes into the *memory*
        sm.st.opcodes.extend_from_slice(&[
            Opcode::LDI(0),
            Opcode::LDI(1),
            Opcode::LDI(2),
            Opcode::RET,
        ]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![321, 394, 0, 1, 2]);
    }

    #[test]
    fn test_execute_drop() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[321, 394]);
        // Put the opcodes into the *memory*
        sm.st.opcodes.extend_from_slice(&[
            Opcode::LDI(0),
            Opcode::LDI(1),
            Opcode::DROP,
            Opcode::LDI(2),
            Opcode::RET,
        ]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![321, 394, 0, 2]);
    }

    #[test]
    #[should_panic]
    fn test_execute_drop_error() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[321, 394]);
        // Put the opcodes into the *memory*
        sm.st.opcodes.extend_from_slice(&[
            Opcode::LDI(0),
            Opcode::LDI(1),
            Opcode::DROP,
            Opcode::DROP,
            Opcode::DROP,
            Opcode::DROP,
            Opcode::DROP,
            Opcode::LDI(2),
            Opcode::RET,
        ]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();
        assert_eq!(sm.st.data_stack, vec![]);
    }

    #[test]
    fn test_execute_swap() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[321, 394]);
        // Put the opcodes into the *memory*
        sm.st.opcodes.extend_from_slice(&[
            Opcode::LDI(0),
            Opcode::LDI(1),
            Opcode::SWAP,
            Opcode::LDI(2),
            Opcode::RET,
        ]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![321, 394, 1, 0, 2]);
    }

    #[test]
    fn test_execute_add() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[123, 321]);
        // Put the opcodes into the *memory*
        sm.st.opcodes.extend_from_slice(&[Opcode::ADD, Opcode::RET]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![444]);

        //noather example
        sm.st.data_stack.clear();
        sm.st.data_stack.extend_from_slice(&[17, 20, 132, 3, 9]);

        sm.st.opcodes.clear();
        sm.st.opcodes.push(Opcode::ADD);
        sm.st.opcodes.push(Opcode::RET);
        sm.execute(0, GasLimit::Limited(100)).unwrap();
        assert_eq!(sm.st.data_stack, vec![17, 20, 132, 12]);

        sm.st.opcodes.clear();
        sm.st.opcodes.push(Opcode::ADD);
        sm.st.opcodes.push(Opcode::RET);
        sm.execute(0, GasLimit::Limited(100)).unwrap();
        assert_eq!(sm.st.data_stack, vec![17, 20, 144]);

        sm.st.opcodes.clear();
        sm.st.opcodes.push(Opcode::ADD);
        sm.st.opcodes.push(Opcode::ADD);
        sm.st.opcodes.push(Opcode::RET);
        println!("{}", sm.execute(0, GasLimit::Limited(100)).unwrap());
        assert_eq!(sm.st.data_stack, vec![181]);
    }

    #[test]
    fn test_execute_sub() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[444, 321]);
        // Put the opcodes into the *memory*
        sm.st.opcodes.extend_from_slice(&[Opcode::SUB, Opcode::RET]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![123]);
    }

    #[test]
    fn test_execute_mul() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[321, 2]);
        // Put the opcodes into the *memory*
        sm.st.opcodes.extend_from_slice(&[Opcode::MUL, Opcode::RET]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![642]);
    }

    #[test]
    fn test_execute_div() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[642, 321]);
        // Put the opcodes into the *memory*
        sm.st.opcodes.extend_from_slice(&[Opcode::DIV, Opcode::RET]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![2]);
    }

    #[test]
    fn test_execute_mod() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[10, 3]);
        // Put the opcodes into the *memory*
        sm.st.opcodes.extend_from_slice(&[Opcode::MOD, Opcode::RET]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![1]);
    }

    #[test]
    fn test_execute_starslash() {
        //    ie  */
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[2000, 34, 100]);
        // Put the opcodes into the *memory*
        sm.st
            .opcodes
            .extend_from_slice(&[Opcode::STARSLASH, Opcode::RET]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![680]);
    }

    #[test]
    fn test_execute_not() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[321, 0]);
        // Put the opcodes into the *memory*
        sm.st.opcodes.extend_from_slice(&[Opcode::NOT, Opcode::RET]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![321, -1]);

        // Put the opcodes into the *memory*
        sm.st.opcodes.extend_from_slice(&[Opcode::NOT, Opcode::RET]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![321, 0]);
    }

    #[test]
    fn test_execute_and() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[15, 1]);
        // Put the opcodes into the *memory*
        sm.st.opcodes.extend_from_slice(&[Opcode::AND, Opcode::RET]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![1]);
    }

    #[test]
    fn test_execute_or() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[8, 7]);
        // Put the opcodes into the *memory*
        sm.st.opcodes.extend_from_slice(&[Opcode::OR, Opcode::RET]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![15]);
    }

    #[test]
    fn test_execute_xor() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[16, 17]);
        // Put the opcodes into the *memory*
        sm.st.opcodes.extend_from_slice(&[Opcode::XOR, Opcode::RET]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![1]);
    }

    #[test]
    fn test_execute_dup() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[123, 394]);
        // Put the opcodes into the *memory*
        sm.st.opcodes.extend_from_slice(&[Opcode::DUP, Opcode::RET]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![123, 394, 394]);
    }

    #[test]
    #[should_panic]
    fn test_execute_run_out_of_gas() {
        let mut sm = StackMachine::new();

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[321, 394]);
        // Put the opcodes into the *memory*
        sm.st.opcodes.extend_from_slice(&[
            Opcode::LDI(0),
            Opcode::LDI(5),
            Opcode::CALL,
            Opcode::LDI(1),
            Opcode::RET,
            Opcode::LDI(2),
            Opcode::LDI(10),
            Opcode::CALL,
            Opcode::LDI(3),
            Opcode::RET,
            Opcode::LDI(4),
            Opcode::LDI(15),
            Opcode::CALL,
            Opcode::LDI(5),
            Opcode::RET,
            Opcode::LDI(6),
            Opcode::LDI(20),
            Opcode::CALL,
            Opcode::LDI(7),
            Opcode::RET,
            Opcode::LDI(8),
            Opcode::LDI(25),
            Opcode::CALL,
            Opcode::LDI(9),
            Opcode::RET,
            Opcode::RET,
        ]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(10)).unwrap();
    }

    #[test]
    fn test_handle_trap_1() {
        let mut sm = StackMachine::new();

        sm.trap_handlers
            .push(Box::from(TrapHandler::new(100, |_trap_id, st| {
                st.data_stack
                    .pop()
                    .ok_or(StackMachineError::NumberStackUnderflow)?;
                st.data_stack.push(200);
                Ok(TrapHandled::Handled)
            })));

        // Populate the number stack
        sm.st.data_stack.extend_from_slice(&[50, 100]);
        // Put the opcodes into the *memory*
        sm.st
            .opcodes
            .extend_from_slice(&[Opcode::TRAP, Opcode::RET]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![200]);
    }

    #[test]
    fn test_handle_trap_2() {
        let mut sm = StackMachine::new();

        sm.trap_handlers
            .push(Box::from(TrapHandler::new(-100, |_trap_id, st| {
                st.data_stack
                    .pop()
                    .ok_or(StackMachineError::NumberStackUnderflow)?;
                st.data_stack.push(-100);
                Ok(TrapHandled::Handled)
            })));
        sm.trap_handlers
            .push(Box::from(TrapHandler::new(100, |_trap_id, st| {
                st.data_stack
                    .pop()
                    .ok_or(StackMachineError::NumberStackUnderflow)?;
                st.data_stack.push(200);
                Ok(TrapHandled::Handled)
            })));
        sm.trap_handlers
            .push(Box::from(TrapHandler::new(-200, |_trap_id, st| {
                st.data_stack
                    .pop()
                    .ok_or(StackMachineError::NumberStackUnderflow)?;
                st.data_stack.push(-200);
                Ok(TrapHandled::Handled)
            })));

        // Populate the number stack, with a value (50), and the trap number (100)
        sm.st.data_stack.extend_from_slice(&[50, 100]);
        // Put the opcodes into the *memory*
        sm.st
            .opcodes
            .extend_from_slice(&[Opcode::TRAP, Opcode::RET]);

        // Execute the instructions
        sm.execute(0, GasLimit::Limited(100)).unwrap();

        assert_eq!(sm.st.data_stack, vec![200]);
    }

    #[test]
    fn test_unhandled_trap_1() {
        let mut sm = StackMachine::new();

        // Populate the number stack, with a value (50), and the trap number (100)
        sm.st.data_stack.extend_from_slice(&[50, 100]);

        // Put the opcodes into the *memory*
        sm.st
            .opcodes
            .extend_from_slice(&[Opcode::TRAP, Opcode::RET]);

        // Execute the instructions
        match sm.execute(0, GasLimit::Limited(100)) {
            Err(StackMachineError::UnhandledTrap) => (),
            r => panic!("Incorrect error type returned {:?}", r),
        }
    }
}
