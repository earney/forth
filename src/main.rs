use forth::forth_compiler::ForthCompiler;
use forth::stack_machine::GasLimit;
use forth::ForthError;
//use std::fs;
use std::{io, io::prelude::*};

use forth::block::Block;

pub fn read_init_file(fc: &mut ForthCompiler) {
    let mut block = Block::new(0);
    block.read(); // read block 0 into memory;
                  //block.display();

    for line in block.data {
        let line = match std::str::from_utf8(&line) {
            Ok(x) => x,
            Err(x) => panic!("{}", x),
        };
        if line.trim() != "" {
            println!("{}", line);
            fc.execute_string(line, GasLimit::Limited(100)).unwrap();
        }
    }

    /*
    // check to see if init.forth exists.  If so, load definitions..
    if fs::metadata("init.forth").is_ok() {
        println!("Reading word definitions from init.forth file");
        let startup = fs::read_to_string("init.forth");
        let _x = match startup {
            Ok(s) => Ok(fc.execute_string(&s, GasLimit::Limited(100)).unwrap()),
            Err(x) => Err(x),
        };
    }
    */
}

fn process_line(fc: &mut ForthCompiler, line: String) -> String {
    match line.as_str() {
        //Ok(n) => match n.as_str() {
        ".quit" => {
            println!("\n Bye!\n");
            std::process::exit(0);
        }
        ".s" => {
            format!("{:?}", fc.sm.st.data_stack)
        }
        "XX" => {
            // clears the parameter stack
            fc.sm.st.data_stack.clear();
            format!("   <{}> ok", fc.sm.st.data_stack.len())
        }
        // editor commands
        "LIST" => {
            let block_number = fc.sm.st.data_stack.pop().unwrap() as usize;
            let block = Block::new(block_number);
            block.display()
            // lists the contents of the block and sets as the current block  (n -- )
            //"".to_string()
        }
        "LOAD" => {
            let block_number = fc.sm.st.data_stack.pop().unwrap() as usize;
            let mut block = Block::new(block_number);
            block.read();

            let mut num_lines = 0;
            for line in block.data {
                let line = match std::str::from_utf8(&line) {
                    Ok(x) => x,
                    Err(x) => panic!("{}", x),
                };
                if line.trim() != "" {
                    println!("{}", line);
                    num_lines += 1;
                    fc.execute_string(line, GasLimit::Limited(100)).unwrap();
                }
            }
            // load (and compile) the contents of the block and set this block as the currnet block (n -- )
            format!("loaded {} lines from block {}.", num_lines, block_number)
        }
        "FLUSH" => {
            // writes the contents of the block to storage (ie, disk)
            "".to_string()
        }
        "COPY" => {
            // copy one block to another (src dest -- )
            "".to_string()
        }
        "WIPE" => {
            // clear the contents of the current block
            "".to_string()
        }
        "WORDS" => {
            //  let words = Vec::from_iter(fc.intrinsic_words.keys());
            //  words.extend(Vec::from(fc.word_addresses.keys()));
            //  words.extend(Vec::from_iter(fc.sm.st.constants.keys()));
            //  words.sort();
            //  words.join(" ")
            "".to_string()
        }
        _ => match fc.execute_string(&line, GasLimit::Limited(200)) {
            Ok(x) => format!(" {}  <{}> ok", x, fc.sm.st.data_stack.len()),
            Err(x) => format!("error: {:?}", x),
        },
    }
    //   Err(error) => format!("error reading stdin: {:}", error),
    // }
}

fn main() -> Result<(), ForthError> {
    println!("Forth..");

    let mut fc = ForthCompiler::new();

    read_init_file(&mut fc);

    println!("Type '.quit' to exit..");

    /*
        let handle = io::stdin();
       // let mut handle = stdin.lock();
        let mut line = String::new();
        let mut eof = false;

        while !eof {
            match handle.read_line(&mut line) {
                Ok(0) => eof = true,
                Ok(_) => {
                    line.pop();
                    process_line(&mut fc, line.clone());
                    line.clear();
                }
                Err(e) => println!("{}", e),
            }
        }
    */

    for line in io::stdin().lock().lines() {
        println!("{}", process_line(&mut fc, line.unwrap()));
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use crate::{process_line, read_init_file};
    use forth::forth_compiler::ForthCompiler;
    use forth::stack_machine::GasLimit;

    #[test]
    fn test_process_line() {
        let mut fc = ForthCompiler::new();

        assert_eq!(process_line(&mut fc, "2 2 + .".to_string()), " 4   <0> ok");
        assert_eq!(process_line(&mut fc, "2 2 .".to_string()), " 2   <1> ok");
        assert_eq!(fc.sm.st.data_stack, vec![2]);
        assert_eq!(process_line(&mut fc, "XX".to_string()), "   <0> ok");
        assert_eq!(fc.sm.st.data_stack, vec![]);
    }

    #[test]
    fn init_file() {
        let mut fc = ForthCompiler::new();
        read_init_file(&mut fc);

        // let see if some of our init forth file defs work.
        assert_eq!(
            fc.execute_string("2 2 + .", GasLimit::Limited(100))
                .unwrap(),
            "4 "
        );

        assert_eq!(
            fc.execute_string("10 PI .", GasLimit::Limited(100))
                .unwrap(),
            "314 "
        );
        assert_eq!(
            fc.execute_string("100 SQRT .", GasLimit::Limited(100))
                .unwrap(),
            "141 "
        );
        assert_eq!(
            fc.execute_string("100 E .", GasLimit::Limited(100))
                .unwrap(),
            "271 "
        );
    }

    #[test]
    fn basic() {
        let mut fc = ForthCompiler::new();

        //fc.execute_string("1 IF 1 2 + ELSE 3 4 + THEN", GasLimit::Limited(100))?;
        fc.execute_string("0 IF 1 2 + THEN", GasLimit::Limited(100))
            .unwrap();

        //println!("Contents of Number Stack {:?}", fc.sm.st.data_stack);
        assert_eq!(&fc.sm.st.data_stack, &vec![]);

        fc.execute_string(": RickTest 1 2 + 3 * ; RickTest", GasLimit::Limited(100))
            .unwrap();

        assert_eq!(&fc.sm.st.data_stack, &vec![9]);

        fc.execute_string(": RickTest2 4 5 + 6 * ;", GasLimit::Limited(100))
            .unwrap();

        assert_eq!(&fc.sm.st.data_stack, &vec![9]);

        fc.execute_string(
            ": RickTest3 RickTest RickTest2 7 + 8 * ;",
            GasLimit::Limited(100),
        )
        .unwrap();

        assert_eq!(&fc.sm.st.data_stack, &vec![9]);

        fc.execute_string("RickTest3", GasLimit::Limited(100))
            .unwrap();

        assert_eq!(&fc.sm.st.data_stack, &vec![9, 9, 488]);

        fc.execute_string("123 321 + 2 *", GasLimit::Limited(100))
            .unwrap();

        assert_eq!(&fc.sm.st.data_stack, &vec![9, 9, 488, 888]);

        fc.execute_string("123 321 + 2 *", GasLimit::Limited(100))
            .unwrap();

        assert_eq!(&fc.sm.st.data_stack, &vec![9, 9, 488, 888, 888]);

        fc.execute_string(
            ": RickCommand 1234 DUP + 777 ; RickCommand RickCommand",
            GasLimit::Limited(100),
        )
        .unwrap();

        fc.sm.st.data_stack.push(123);
        fc.sm.st.data_stack.push(321);
        fc.sm.st.data_stack.push(0);
        fc.execute_string("IF + 2 * ELSE + 3 * THEN", GasLimit::Limited(100))
            .unwrap();
        let n = fc.sm.st.data_stack.pop().unwrap();

        assert_eq!(n, 1332);
    }
}
