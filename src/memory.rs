type StackType = i16;
const STACK_LENGTH: usize = (StackType::BITS / u8::BITS) as usize; // in memory we hold u8's so we need to know how wide a STackType is.

pub struct CellInfo {
    pub isconstant: bool,
    pub memory_address: Option<usize>,
    pub length: usize,
}

pub struct Memory {
    pub variables: Vec<String>, // name, addr
    pub cell_type_array: Vec<CellInfo>,
    pub data: Vec<u8>,
    pos: usize,
    pub last_created_word_addr: Option<usize>,
}

impl Default for Memory {
    fn default() -> Self {
        Self::new()
    }
}

impl Memory {
    pub fn new() -> Memory {
        Memory {
            variables: Vec::new(),
            cell_type_array: Vec::new(),
            data: Vec::new(),
            pos: 0,
            last_created_word_addr: None,
        }
    }
}

impl Memory {
    pub fn allocate(&mut self, word_addr: usize, len: usize) -> usize {
        println!(
            "allocate(word_addr:{}, len:{}, memory_address:{})",
            word_addr, len, self.pos
        );
        // assume this word is new and give it a new address at the end of the allocated memory
        self.last_created_word_addr = Some(word_addr);
        self.cell_type_array[word_addr].memory_address = Some(self.pos);
        self.cell_type_array[word_addr].length = len;

        for _i in 0..len * STACK_LENGTH {
            self.data.push(0);
            self.pos += 1;
        }

        self.cell_type_array[word_addr].memory_address.unwrap() // return address of this variable
    }

    pub fn allocate_byte_and_set_value(&mut self, value: StackType) {
        println!("allocate_byte_and_set_value({}) pos:{}", value, self.pos);
        let word_addr = self.last_created_word_addr.unwrap() as usize;
        if self.cell_type_array[word_addr].memory_address.is_none() {
            self.cell_type_array[word_addr].memory_address = Some(self.pos);
        }
        //self.pos = self.data.len();
        let u8_data = StackType::to_le_bytes(value);
        for item in u8_data.iter() {
            self.data.push(*item);
            self.pos += 1;
        }
        self.cell_type_array[word_addr].length += 1;
    }

    pub fn add_variable(&mut self, name: String, isconstant: bool) -> usize {
        println!("add_variable(name:{}, isconstant:{})", name, isconstant);
        let len = self.variables.len();
        self.variables.push(name);
        self.cell_type_array.push(CellInfo {
            isconstant,
            memory_address: None,
            length: 0,
        });
        println!("len:{}", len);
        len
    }

    pub fn get_word_addr(&mut self, name: String) -> Option<usize> {
        (0..self.variables.len()).find(|&i| self.variables[i] == name.to_uppercase())
    }

    pub fn is_constant(&mut self, name: String) -> bool {
        match self.get_word_addr(name) {
            Some(x) => self.cell_type_array[x].isconstant,
            None => false,
        }
    }

    pub fn contains_word(&mut self, name: String) -> bool {
        self.get_word_addr(name).is_some()
    }

    pub fn length(&self) -> usize {
        self.pos
    }

    pub fn set_memory_address(&mut self, data: Vec<StackType>, memory_address: usize) -> bool {
        println!("set_memory_address({})", memory_address);
        let mut pos = memory_address;

        for item in &data {
            let u8_data = StackType::to_le_bytes(*item);
            for item in u8_data.iter() {
                if pos >= self.data.len() {
                    self.data.push(*item);
                    self.pos += 1;
                    pos += 1;
                    println!("push");
                } else {
                    self.data[pos] = *item;
                    pos += 1;
                    println!("replace");
                }
            }
        }
        true
    }

    pub fn set(&mut self, data: Vec<StackType>, word_addr: usize) -> bool {
        println!("set({:?}, {})", data, word_addr);
        let mut pos = self.cell_type_array[word_addr].memory_address.unwrap();
        let allocated_length = self.cell_type_array[word_addr].length;
        if allocated_length < data.len() {
            for item in &data {
                let u8_data = StackType::to_le_bytes(*item);
                for item in u8_data.iter() {
                    self.data.push(*item);
                    pos += 1;
                }
            }
            self.cell_type_array[word_addr].length += data.len();
        } else {
            for item in &data {
                let u8_data = StackType::to_le_bytes(*item);
                for item in u8_data.iter() {
                    self.data[pos] = *item;
                    pos += 1;
                }
            }
        }

        if allocated_length > data.len() {
            for i in data.len()..allocated_length {
                self.data[i] = 0;
            }
        }
        true
    }

    pub fn get(&self, word_addr: usize) -> Vec<StackType> {
        println!("get({})", word_addr);
        let addr = self.cell_type_array[word_addr].memory_address.unwrap(); // first byte contains the length of the variable/constant
        let len = self.cell_type_array[word_addr].length;

        // now get data
        let mut value: Vec<StackType> = Vec::new();
        for i in (0..len * STACK_LENGTH).step_by(STACK_LENGTH) {
            let x: usize = i as usize + addr;
            let a: [u8; STACK_LENGTH] = self.data[x..x + STACK_LENGTH].try_into().unwrap();
            value.push(StackType::from_le_bytes(a));
        }
        value
    }

    pub fn get_memory_address(&self, memory_addr: usize) -> Vec<StackType> {
        println!("get_memory_address({})", memory_addr);
        println!("memory data = {:?}", self.data);
        let mut value: Vec<StackType> = Vec::new();
        let a: [u8; STACK_LENGTH] = self.data[memory_addr..memory_addr + STACK_LENGTH]
            .try_into()
            .unwrap();

        value.push(StackType::from_le_bytes(a));
        println!("{:?} {}", a, StackType::from_le_bytes(a));
        value
    }
}
