# forth
Minimal Forth implementation written in Rust

[<img src="https://earney.codeberg.page/badges/flat.svg">](https://earney.codeberg.page/)

Forked from rickprice/rust-forth on github.

Modifying code to add extra functionality and to be near the 1979 Forth Standard.
Will try to get this to compile for older hardware (z80, 6502, 8-bit, and 16-bit machines (as well as modern machines)) using llvm-mos, etc.

I plan on implementing most of the [Forth 79 standard](https://www.physics.wisc.edu/~lmaurer/forth/Forth-79.pdf)

Extra Resources:

+ [Starting Forth](https://www.forth.com/wp-content/uploads/2018/01/Starting-FORTH.pdf) Free PDF of best introduction book on Forth
+ [Thinking Forth](http://www.dnd.utwente.nl/~tim/colorforth/Leo-Brodie/thinking-forth.pdf) Free PDF of best introduction book on Forth mindset
+ [Programming Forth](http://www.mpeforth.com/arena/ProgramForth.pdf) Free PDF
+ [Beginners Guide](http://galileo.phys.virginia.edu/classes/551.jvn.fall01/primer.htm)
+ [Forth Primer](http://ficl.sourceforge.net/pdf/Forth_Primer.pdf)
+ [Forth Encyclopedia - The Complete Forth Programmers Manual](https://archive.org/details/MitchDerickLindaBakerForthEncyclopediaTheCompleteForthProgrammersManualMountainViewPress1982/page/n3/mode/2up)
+ [Forth 79 Standard](https://www.complang.tuwien.ac.at/forth/fth79std/FORTH-79.TXT)